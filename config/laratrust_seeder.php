<?php

return [
    'role_structure' => [
        'super_admin' => [
            'users' => 'c,r,u,d',
            'categories' => 'c,r,u,d',
            'narrations' => 'c,r,u,d',
            'scientists' => 'c,r,u,d',
            'book_references' => 'c,r,u,d',
            'indicator_books' => 'c,r,u,d',
            'speech_orbits' => 'c,r,u,d',
            'provisions' => 'c,r,u,d',
            'narration_fields' => 'c,r,u,d',
            'classifications' => 'c,r,u,d',
            'users' => 'c,r,u,d',
        ],
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ]
];
