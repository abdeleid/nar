<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provision extends Model
{
    protected $guarded = [];

    public function narrations()
    {
        return $this->belongsToMany(Narration::class, 'narration_provision')
            ->withPivot('id', 'order')
            ->orderBy('order', 'asc');

    }//end of narrations

}//end of model
