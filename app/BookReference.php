<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookReference extends Model
{
    protected $guarded = [];

    public function narrations()
    {
        return $this->belongsToMany(Narration::class, 'narration_book_reference')
            ->withPivot('id', 'part', 'page', 'order')
            ->orderBy('order', 'asc');

    }//end of narrations

}//end of model
