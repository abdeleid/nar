<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Speech extends Model
{
    protected $guarded = [];

    public function narration()
    {
        return $this->belongsTo(Narration::class);

    }//end of narration

    public function category()
    {
        return $this->belongsTo(Category::class);

    }//end of category

}//end of model
