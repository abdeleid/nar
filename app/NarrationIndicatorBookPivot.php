<?php

namespace App;


use Illuminate\Database\Eloquent\Relations\Pivot;

class NarrationIndicatorBookPivot extends Pivot
{
    protected $table = 'narration_indicator_book';
    public $timestamps = false;

}//end of model
