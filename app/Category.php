<?php

namespace App;

use App\Scopes\SortedScope;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new SortedScope);

    }//end of boot

    public function speeches()
    {
        return $this->hasMany(Speech::class);

    }//end of speeches

    public function narration_fields()
    {
        return $this->belongsToMany(NarrationField::class, 'category_narration_field');

    }//end fo narration fields

    public function has_narration_field($narration_field_id)
    {
        return in_array($narration_field_id, $this->narration_fields()->pluck('id')->toArray());

    }//end of has narration field

}//end of model
