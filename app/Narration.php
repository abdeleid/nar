<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Narration extends Model
{
    protected $guarded = [];

    //functions -----------------------------------------------------------------------------------------------------------------------

    public function formula($narration_field_id = null)
    {
        if ($narration_field_id) {

            $group_summary = DB::table('speeches')
                ->select('speech_number', 'secondary_number', 'categories.name')
                ->join('categories', 'speeches.category_id', 'categories.id')
                ->join('category_narration_field', 'categories.id', 'category_narration_field.category_id')
                ->where('speeches.narration_id', $this->id)
                ->where('category_narration_field.narration_field_id', $narration_field_id)
                ->get()->groupBy('name');

        } else {

            $group_summary = DB::table('speeches')
                ->select('speech_number', 'secondary_number', 'categories.name')
                ->join('categories', 'speeches.category_id', 'categories.id')
                ->where('speeches.narration_id', $this->id)
                ->get()->groupBy('name');

        }//end of else

        $summary = '';

        foreach ($group_summary as $key => $speeches) {

            $summary .= $key . ': ';

            foreach ($speeches as $index => $speech) {

                if ($speech->secondary_number) $summary .= $speech->secondary_number . '/';

                $summary .= $speech->speech_number;

                if ($index < $speeches->count() - 1) $summary .= '، ';

            }//end of for each

            $summary .= ' ';

        }//end of for each

        return $summary;

    }//end of get attribute

    //relations ------------------------------------------------------------------------------------------------------------------------

    public function user()
    {
        return $this->belongsTo(User::class);

    }//end of user

    public function alerts()
    {
        return $this->hasMany(Alert::class)->orderBy('order', 'asc');

    }//end of classifications

    public function narration_field()
    {
        return $this->belongsToMany(NarrationField::class);

    }//end of narration field

    public function speeches()
    {
        return $this->hasMany(Speech::class)->orderBy('order', 'asc');

    }//end of speeches

    public function scientists()
    {
        return $this->belongsToMany(Scientist::class, 'narration_scientist')
            ->withPivot('id', 'order')
            ->orderBy('order', 'asc');

    }//end of scientists

    public function book_references()
    {
        return $this->belongsToMany(BookReference::class, 'narration_book_reference')
            ->withPivot('id', 'part', 'page', 'order')
            ->orderBy('order', 'asc');

    }//end of book references

    public function provisions()
    {
        return $this->belongsToMany(Provision::class, 'narration_provision')
            ->withPivot('id', 'order')
            ->orderBy('order', 'asc');

    }//end of provisions

    public function scientist_provisions()
    {
        return $this->hasMany(ScientistProvision::class)->orderBy('order', 'asc');

    }//end of scientist provisions

    public function indicator_books()
    {
        return $this->belongsToMany(IndicatorBook::class, 'narration_indicator_book')
            ->withPivot('id', 'order', 'part', 'page', 'speech_number')
            ->orderBy('order', 'asc');

    }//end of indicator books

    public function speech_orbits()
    {
        return $this->belongsToMany(SpeechOrbit::class, 'narration_speech_orbit')
            ->withPivot('id', 'order', 'notes')
            ->orderBy('order', 'asc');

    }//end of speech orbits

}//end of model
