<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{
    protected $guarded = [];

    public function classification()
    {
        return $this->belongsTo(Classification::class);

    }//end of classification

}//end of model
