<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScientistProvision extends Model
{
    protected $guarded = [];

    public function narration()
    {
        return $this->belongsTo(Narration::class);

    }//end of narration

    public function scientist()
    {
        return $this->belongsTo(Scientist::class);

    }//end of scientist

    public function provision()
    {
        return $this->belongsTo(Provision::class);

    }//end of provision

    public function book_reference()
    {
        return $this->belongsTo(BookReference::class);

    }//end of book reference

}//end of model
