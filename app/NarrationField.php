<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NarrationField extends Model
{
    protected $guarded = [];

    //functions -------------------------------------------------------------------------------------------------------------------------

    public function sentence($narration_id)
    {
        $narration = Narration::FindOrFail($narration_id);
        //$grouped_speeches = $narration->speeches->groupBy('category_id');
        //
        //
        //foreach ($grouped_speeches as $grouped_speech) {
        //
        //    if ($grouped_speech->count() > 1) {
        //
        //    } else {
        //
        //        foreach ($grouped_speech as $speech) {
        //
        //            dd($speech->category->name);
        //
        //        }//end of for each
        //
        //    }
        //
        //}//end of for each


        //$result = [];
        //


        //dd($speeches);


        //$merged_collection = new Collection();
        //
        //foreach ($speeches as $key => $speech) {
        //
        //    $merged_collection->merge($speech);
        //
        //}//end of for each

        //dd($merged_collection);
        //dd($speeches);
        //dd(array_merge($speeches->toArray()));

        //$speeches_count = count($speeches);
        //
        //$result = $this->name . ': ';

        //foreach ($speeches as $index => $speech) {
        //
        //    if ($speech->secondary_number) $result .= $speech->secondary_number . '/';
        //
        //    $result .= $speech->speech_number;
        //
        //
        //    if ($index < $speeches_count - 1) $result .= ', ';
        //
        //}//end of for each

        return $result;

    }//end of sentence

    //relations -------------------------------------------------------------------------------------------------------------------------

    public function narrations()
    {
        return $this->hasMany(Narration::class);

    }//end of narrations

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_narration_field');

    }//end of categories

}//end of model
