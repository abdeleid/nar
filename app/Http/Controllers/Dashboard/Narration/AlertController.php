<?php

namespace App\Http\Controllers\Dashboard\Narration;

use App\Alert;
use App\BookReference;
use App\Classification;
use App\Http\Controllers\Controller;
use App\Narration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlertController extends Controller
{
    public function create(Narration $narration)
    {
        $classifications = Classification::where('type', 'alerts')->get();
        $book_references = BookReference::all();
        return view('dashboard.narrations.alerts._create', compact(
            'narration',
            'classifications',
            'book_references'));

    }//end of create

    public function store(Request $request, Narration $narration)
    {
        $request->validate([
            'classification_id' => 'required_without:new_classification',
            'new_classification' => 'required_without:classification_id|unique:classifications,name',
            'alert' => 'required',

            'book_reference_id' => 'required_without:new_book_reference',
            'new_book_reference' => 'required_without:book_reference_id|unique:book_references,name',
            'part' => 'required',
            'page' => 'required'
        ]);

        $alert = $narration->alerts()->create([]);

        $this->check_data($request, $alert);

        session()->flash('success', __('site.added_successfully'));
        return view('dashboard.narrations.alerts._index', compact('narration'));

    }//end of store

    public function sort(Request $request, Narration $narration)
    {
        foreach ($request->ids as $index => $id) {

            DB::table('alerts')->where('id', $id)->update(['order' => $index + 1]);

        }//end of for each

        session()->flash('success', __('site.sorted_successfully'));
        return view('dashboard.narrations.alerts._index', compact('narration'));

    }//end of sort

    public function edit(Narration $narration, Alert $alert)
    {
        $classifications = Classification::where('type', 'alerts')->get();
        $book_references = BookReference::all();

        return view('dashboard.narrations.alerts._edit',
            compact('narration',
                'classifications',
                'alert',
                'book_references'));

    }//end of edit

    public function update(Request $request, Narration $narration, Alert $alert)
    {
        $request->validate([
            'classification_id' => 'required_without:new_classification',
            'new_classification' => 'required_without:classification_id|unique:classifications,name',
            'alert' => 'required',

            'book_reference_id' => 'required_without:new_book_reference',
            'new_book_reference' => 'required_without:book_reference_id|unique:book_references,name',
            'part' => 'required',
            'page' => 'required'
        ]);

        $this->check_data($request, $alert);

        session()->flash('success', __('site.updated_successfully'));
        return view('dashboard.narrations.alerts._index', compact('narration'));

    }//end of update

    public function destroy(Narration $narration, Alert $alert)
    {
        $alert->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return view('dashboard.narrations.alerts._index', compact('narration'));

    }//end of destroy

    private function check_data($request, $alert)
    {
        if ($request->filled('new_classification')) {

            $new_classification = Classification::create([
                'name' => $request->new_classification,
                'type' => $request->type,
            ]);

        }//end of if

        if ($request->filled('new_book_reference')) {

            $new_book_reference = BookReference::create(['name' => $request->new_book_reference]);

        }//end of if

        $alert->update([
            'classification_id' => $new_classification->id ?? $request->classification_id,
            'alert' => $request->alert,
            'book_reference_id' => $new_book_reference->id ?? $request->book_reference_id,
            'booK_reference_part' => $request->part,
            'booK_reference_page' => $request->page,
            'det' => $request->det,
        ]);

    }//end of check data

    //    private function add_new_classification($request, $narration, $alert = null)
    //    {
    //        $new_classification = Classification::create(['name' => $request->name, 'type' => $request->type]);
    //
    //        if (!$alert) {
    //
    //            $narration->alerts()->create([
    //                'classification_id' => $new_classification->id,
    //                'alert' => $request->alert,
    ////                'ref' => $request->ref,
    ////                'det' => $request->det
    //            ]);
    //
    //        } else {
    //
    //            $alert->update([
    //                'classification_id' => $new_classification->id,
    //                'alert' => $request->alert,
    //                'ref' => $request->ref,
    //                'det' => $request->det
    //            ]);
    //
    //        }//end of else
    //
    //    }//end of add new classification
    //
    //    private function attach_classification($request, $narration, $alert = null)
    //    {
    //        if (!$alert) {
    //
    //            $narration->alerts()->create([
    //                'classification_id' => $request->classification_id,
    //                'alert' => $request->alert,
    //                'ref' => $request->ref,
    //                'det' => $request->det
    //            ]);
    //
    //        } else {
    //
    //            $alert->update([
    //                'classification_id' => $request->classification_id,
    //                'alert' => $request->alert,
    //                'ref' => $request->ref,
    //                'det' => $request->det
    //            ]);
    //
    //        }//end of else
    //
    //    }//end of attach classification

}//end of controller
