<?php

namespace App\Http\Controllers\Dashboard\Narration;

use App\Narration;
use App\Provision;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ProvisionController extends Controller
{
    public function create(Narration $narration)
    {
        $provisions = Provision::all();
        return view('dashboard.narrations.provisions._create', compact('narration', 'provisions'));

    }//end of create

    public function store(Request $request, Narration $narration)
    {
        if ($request->filled('name')) {

            $request->validate([
                'name' => 'required_without:provision_id|unique:provisions',
            ]);

            $this->add_new_provision($request, $narration);

        } else {

            $request->validate([
                'provision_id' => ['required_without:name', Rule::unique('narration_provision')->where(function ($query) use ($narration) {
                    return $query->whereIn('provision_id', $narration->provisions()->pluck('provision_id')->toArray());
                })]
            ]);

            $this->attach_provision($request, $narration);

        }//end of else

        session()->flash('success', __('site.added_successfully'));
        return view('dashboard.narrations.provisions._index', compact('narration'));

    }//end of store

    private function add_new_provision($request, $narration, $provision = null)
    {
        $new_provision = Provision::create(['name' => $request->name]);

        if (!$provision) {

            $narration->provisions()->attach($new_provision->id);

        } else {

            $narration->provisions()->updateExistingPivot($provision->id, [
                'provision_id' => $new_provision->id
            ]);

        }//end of else

    }//end of add new provision

    private function attach_provision($request, $narration, $provision = null)
    {
        if (!$provision) {

            $narration->provisions()->attach($request->provision_id);

        } else {

            $narration->provisions()->updateExistingPivot($provision->id, [
                'provision_id' => $request->provision_id
            ]);

        }//end of else

    }//end of attach provision

    public function sort(Request $request, Narration $narration)
    {
        foreach ($request->ids as $index => $id) {

            DB::table('narration_provision')->where('id', $id)->update(['order' => $index + 1]);

        }//end of for each

        session()->flash('success', __('site.sorted_successfully'));
        return view('dashboard.narrations.provisions._index', compact('narration'));

    }//end of sort

    public function edit(Narration $narration, Provision $provision)
    {
        $provisions = Provision::all();
        $narration_provision = $narration->provisions()->where('provision_id', $provision->id)->first();

        return view('dashboard.narrations.provisions._edit',
            compact('narration',
                'provisions',
                'provision',
                'narration_provision'
            ));

        return view('dashboard.narrations.provisions._edit', compact('narration', 'provisions', 'provision'));

    }//end of edit

    public function update(Request $request, Narration $narration, Provision $provision)
    {
        if ($request->filled('name')) {

            $request->validate([
                'name' => 'required_without:provision_id|unique:provisions',
            ]);

            $this->add_new_provision($request, $narration, $provision);

        } else {

            $request->validate([
                'provision_id' => [
                    'required_without:name',
                    Rule::unique('narration_provision')->where(function ($query) use ($narration) {
                        return $query->whereIn('provision_id', $narration->provisions()->pluck('provision_id')->toArray());
                    })->ignore($provision->id, 'provision_id')
                ]
            ]);

            $this->attach_provision($request, $narration, $provision);

        }//end of else

        session()->flash('success', __('site.updated_successfully'));
        return view('dashboard.narrations.provisions._index', compact('narration'));

    }//end of update

    public function destroy(Narration $narration, Provision $provision)
    {
        $narration->provisions()->detach($provision->id);
        session()->flash('success', __('site.deleted_successfully'));
        return view('dashboard.narrations.provisions._index', compact('narration'));

    }//end of destroy
    
}//end of controller
