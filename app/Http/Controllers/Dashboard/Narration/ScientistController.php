<?php

namespace App\Http\Controllers\Dashboard\Narration;

use App\Narration;
use App\Scientist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class ScientistController extends Controller
{
    public function create(Narration $narration)
    {
        $scientists = Scientist::all();
        return view('dashboard.narrations.scientists._create', compact('narration', 'scientists'));

    }//end of create

    public function store(Request $request, Narration $narration)
    {
        if ($request->filled('name')) {

            $request->validate([
                'name' => 'required_without:scientist_id|unique:scientists',
            ]);

            $this->add_new_scientist($request, $narration);

        } else {

            $request->validate([
                'scientist_id' => ['required_without:name', Rule::unique('narration_scientist')->where(function ($query) use ($narration) {
                    return $query->whereIn('scientist_id', $narration->scientists()->pluck('scientist_id')->toArray());
                })]
            ]);

            $this->attach_scientist($request, $narration);

        }//end of else

        session()->flash('success', __('site.added_successfully'));
        return view('dashboard.narrations.scientists._index', compact('narration'));

    }//end of store

    public function sort(Request $request, Narration $narration)
    {
        foreach ($request->ids as $index => $id) {

            DB::table('narration_scientist')->where('id', $id)->update(['order' => $index + 1]);

        }//end of for each

        session()->flash('success', __('site.sorted_successfully'));
        return view('dashboard.narrations.scientists._index', compact('narration'));

    }//end of sort

    public function edit(Narration $narration, Scientist $scientist)
    {
        $scientists = Scientist::all();
        $narration_scientist = $narration->scientists()->where('scientist_id', $scientist->id)->first();

        return view('dashboard.narrations.scientists._edit',
            compact('narration',
                'scientists',
                'scientist',
                'narration_scientist'
            ));

        return view('dashboard.narrations.scientists._edit', compact('narration', 'scientists', 'scientist'));

    }//end of edit

    public function update(Request $request, Narration $narration, Scientist $scientist)
    {
        if ($request->filled('name')) {

            $request->validate([
                'name' => 'required_without:scientist_id|unique:scientists',
            ]);

            $this->add_new_scientist($request, $narration, $scientist);

        } else {

            $request->validate([
                'scientist_id' => [
                    'required_without:name',
                    Rule::unique('narration_scientist')->where(function ($query) use ($narration) {
                        return $query->whereIn('scientist_id', $narration->scientists()->pluck('scientist_id')->toArray());
                    })->ignore($scientist->id, 'scientist_id')
                ]
            ]);

            $this->attach_scientist($request, $narration, $scientist);

        }//end of else

        session()->flash('success', __('site.updated_successfully'));
        return view('dashboard.narrations.scientists._index', compact('narration'));

    }//end of update

    public function destroy(Narration $narration, Scientist $scientist)
    {
        $narration->scientists()->detach($scientist->id);
        session()->flash('success', __('site.deleted_successfully'));
        return view('dashboard.narrations.scientists._index', compact('narration'));

    }//end of destroy

    private function add_new_scientist($request, $narration, $scientist = null)
    {
        $new_scientist = Scientist::create(['name' => $request->name]);

        if (!$scientist) {

            $narration->scientists()->attach($new_scientist->id);

        } else {

            $narration->scientists()->updateExistingPivot($scientist->id, [
                'scientist_id' => $new_scientist->id
            ]);

        }//end of else

    }//end of add new scientist

    private function attach_scientist($request, $narration, $scientist = null)
    {
        if (!$scientist) {

            $narration->scientists()->attach($request->scientist_id);

        } else {

            $narration->scientists()->updateExistingPivot($scientist->id, [
                'scientist_id' => $request->scientist_id
            ]);

        }//end of else

    }//end of attach scientist

}//end of controller
