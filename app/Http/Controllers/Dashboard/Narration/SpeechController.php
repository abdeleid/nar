<?php

namespace App\Http\Controllers\Dashboard\Narration;

use App\Category;
use App\Http\Controllers\Controller;
use App\Narration;
use App\Speech;
use Illuminate\Http\Request;

class SpeechController extends Controller
{
    public function index(Narration $narration)
    {

    }//end of index

    public function create(Narration $narration)
    {
        $categories = Category::all();
        return view('dashboard.narrations.speeches._create', compact('narration', 'categories'));

    }//end of create

    public function store(Request $request, Narration $narration)
    {
        $request->validate([
            'category_id' => 'required|exists:categories,id',
            'speech_number' => 'sometimes|nullable|integer',
            'part_number' => 'sometimes|nullable|integer',
            'page_number' => 'sometimes|nullable|integer',
            'secondary_number' => 'sometimes|nullable|integer',
        ]);

        $narration->speeches()->create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return view('dashboard.narrations.speeches._index', compact('narration'));

    }//end of store

    public function sort(Request $request, Narration $narration, Speech $speech)
    {
        foreach ($request->ids as $index => $id) {

            $speech = Speech::FindOrFail($id);
            $speech->update(['order' => $index + 1]);

        }//end of for each

        session()->flash('success', __('site.sorted_successfully'));
        return view('dashboard.narrations.speeches._index', compact('narration'));

    }//end of sort

    public function edit(Narration $narration, Speech $speech)
    {
        $categories = Category::all();
        return view('dashboard.narrations.speeches._edit', compact('categories', 'narration', 'speech'));

    }//end of edit

    public function update(Request $request, Narration $narration, Speech $speech)
    {
        $request->validate([
            'category_id' => 'required|exists:categories,id',
            'speech_number' => 'sometimes|nullable|integer',
            'part_number' => 'sometimes|nullable|integer',
            'page_number' => 'sometimes|nullable|integer',
            'secondary_number' => 'sometimes|nullable|integer',
        ]);

        $speech->update($request->all());
        session()->flash('success', __('site.updated_successfully'));
        return view('dashboard.narrations.speeches._index', compact('narration'));

    }//end of update

    public function destroy(Narration $narration, Speech $speech)
    {
        $speech->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return view('dashboard.narrations.speeches._index', compact('narration'));

    }//end of destroy

}//end of controller
