<?php

namespace App\Http\Controllers\Dashboard\Narration;

use App\BookReference;
use App\Narration;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class BookReferenceController extends Controller
{
    public function create(Narration $narration)
    {
        $book_references = BookReference::all();
        return view('dashboard.narrations.book_references._create', compact('narration', 'book_references'));

    }//end of create

    public function store(Request $request, Narration $narration)
    {
        if ($request->filled('name')) {

            $request->validate([
                'name' => 'required_without:book_reference_id|unique:book_references',
                'part' => 'required',
                'page' => 'required',
            ]);

            $this->add_new_book_reference($request, $narration);

        } else {

            $request->validate([
                'book_reference_id' => ['required_without:name', Rule::unique('narration_book_reference')->where(function ($query) use ($narration) {
                    return $query->whereIn('book_reference_id', $narration->book_references()->pluck('book_reference_id')->toArray());
                })],
                'part' => 'required',
                'page' => 'required',
            ]);

            $this->attach_book_reference($request, $narration);

        }//end of else

        session()->flash('success', __('site.added_successfully'));
        return view('dashboard.narrations.book_references._index', compact('narration'));

    }//end of store

    private function add_new_book_reference($request, $narration, $book_reference = null)
    {
        $new_book_reference = BookReference::create(['name' => $request->name]);

        if (!$book_reference) {

            $narration->book_references()->attach($new_book_reference->id, [
                'part' => $request->part,
                'page' => $request->page
            ]);

        } else {

            $narration->book_references()->updateExistingPivot($book_reference->id, [
                'book_reference_id' => $new_book_reference->id,
                'part' => $request->part,
                'page' => $request->page
            ]);

        }//end of else

    }//end of add new book_reference

    private function attach_book_reference($request, $narration, $book_reference = null)
    {
        if (!$book_reference) {

            $narration->book_references()->attach($request->book_reference_id, [
                'part' => $request->part,
                'page' => $request->page
            ]);

        } else {

            $narration->book_references()->updateExistingPivot($book_reference->id, [
                'book_reference_id' => $request->book_reference_id,
                'part' => $request->part,
                'page' => $request->page,
            ]);

        }//end of else

    }//end of attach book_reference

    public function sort(Request $request, Narration $narration)
    {
        foreach ($request->ids as $index => $id) {

            DB::table('narration_book_reference')->where('id', $id)->update(['order' => $index + 1]);

        }//end of for each

        session()->flash('success', __('site.sorted_successfully'));
        return view('dashboard.narrations.book_references._index', compact('narration'));

    }//end of sort

    public function edit(Narration $narration, BookReference $book_reference)
    {
        $book_references = BookReference::all();
        $narration_book_reference = $narration->book_references()->where('book_reference_id', $book_reference->id)->first();

        return view('dashboard.narrations.book_references._edit',
            compact('narration',
                'book_references',
                'book_reference',
                'narration_book_reference'
            ));

    }//end of edit

    public function update(Request $request, Narration $narration, BookReference $book_reference)
    {
        if ($request->filled('name')) {

            $request->validate([
                'book-name' => 'required_without:book_reference_id|unique:book_references',
                'part' => 'required',
                'page' => 'required'
            ]);

            $this->add_new_book_reference($request, $narration, $book_reference);

        } else {

            $request->validate([
                'book_reference_id' => [
                    'required_without:name',
                    Rule::unique('narration_book_reference')->where(function ($query) use ($narration) {
                        return $query->whereIn('book_reference_id', $narration->book_references()->pluck('book_reference_id')->toArray());
                    })->ignore($book_reference->id, 'book_reference_id')
                ],
                'part' => 'required',
                'page' => 'required'
            ]);

            $this->attach_book_reference($request, $narration, $book_reference);

        }//end of else

        session()->flash('success', __('site.updated_successfully'));
        return view('dashboard.narrations.book_references._index', compact('narration'));

    }//end of update

    public function destroy(Narration $narration, BookReference $book_reference)
    {
        $narration->book_references()->detach($book_reference->id);
        session()->flash('success', __('site.deleted_successfully'));
        return view('dashboard.narrations.book_references._index', compact('narration'));

    }//end of destroy

}//end of controller
