<?php

namespace App\Http\Controllers\Dashboard\Narration;

use App\BookReference;
use App\Http\Controllers\Controller;
use App\Narration;
use App\Provision;
use App\Scientist;
use App\ScientistProvision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ScientistProvisionController extends Controller
{
    public function create(Narration $narration)
    {
        $scientists = Scientist::all();
        $provisions = Provision::all();
        $book_references = BookReference::all();

        return view('dashboard.narrations.scientist_provisions._create',
            compact(
                'narration',
                'scientists',
                'provisions',
                'book_references'
            )
        );

    }//end of create

    public function store(Request $request, Narration $narration)
    {
        $request->validate([
            'scientist_id' => 'required_without:new_scientist',
            'new_scientist' => 'required_without:scientist_id|unique:scientists,name',

            'provision_id' => 'required_without:new_provision',
            'new_provision' => 'required_without:provision_id|unique:scientists,name',

            'book_reference_id' => 'required_without:new_book_reference',
            'new_book_reference' => 'required_without:book_reference_id|unique:book_references,name',
            'book_reference_page' => 'required|integer',
        ]);

        $scientist_provision = $narration->scientist_provisions()->create([]);

        $this->check_data($request, $scientist_provision);

        session()->flash('success', __('site.added_successfully'));
        return view('dashboard.narrations.scientist_provisions._index', compact('narration'));

    }//end of store

    public function sort(Request $request, Narration $narration)
    {
        foreach ($request->ids as $index => $id) {

            DB::table('scientist_provisions')->where('id', $id)->update(['order' => $index + 1]);

        }//end of for each

        session()->flash('success', __('site.sorted_successfully'));
        return view('dashboard.narrations.scientist_provisions._index', compact('narration'));

    }//end of sort

    public function edit(Narration $narration, ScientistProvision $scientist_provision)
    {
        $scientists = Scientist::all();
        $provisions = Provision::all();
        $book_references = BookReference::all();

        return view('dashboard.narrations.scientist_provisions._edit',
            compact(
                'narration',
                'scientists',
                'provisions',
                'book_references',
                'scientist_provision'
            )
        );

    }//end of edit

    public function update(Request $request, Narration $narration, ScientistProvision $scientist_provision)
    {
        $request->validate([
            'scientist_id' => 'required_without:new_scientist',
            'new_scientist' => 'required_without:scientist_id',

            'provision_id' => 'required_without:new_provision',
            'new_provision' => 'required_without:provision_id',

            'book_reference_id' => 'required_without:new_book_reference',
            'new_book_reference' => 'required_without:book_reference_id',
            'book_reference_page' => 'required|integer',
        ]);

        $this->check_data($request, $scientist_provision);

        session()->flash('success', __('site.updated_successfully'));
        return view('dashboard.narrations.scientist_provisions._index', compact('narration'));

    }//end of update

    public function destroy(Narration $narration, ScientistProvision $scientist_provision)
    {
        $scientist_provision->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return view('dashboard.narrations.scientist_provisions._index', compact('narration'));

    }//end of destroy

    private function check_data($request, $scientist_provision)
    {
        if ($request->filled('new_scientist')) {

            $new_scientist = Scientist::create(['name' => $request->new_scientist]);

        }//end of if

        if ($request->filled('new_provision')) {

            $new_provision = Provision::create(['name' => $request->new_provision]);

        }//end of if

        if ($request->filled('new_book_reference')) {

            $new_book_reference = BookReference::create(['name' => $request->new_book_reference]);

        }//end of if

        $scientist_provision->update([
            'scientist_id' => $new_scientist->id ?? $request->scientist_id,
            'provision_id' => $new_provision->id ?? $request->provision_id,
            'book_reference_id' => $new_book_reference->id ?? $request->book_reference_id,
            'booK_reference_part' => $request->book_reference_part,
            'booK_reference_page' => $request->book_reference_page,
            'notes' => $request->notes
        ]);

    }//end of check data

}//end of controller
