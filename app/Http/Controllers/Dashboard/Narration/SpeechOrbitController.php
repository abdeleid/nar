<?php

namespace App\Http\Controllers\Dashboard\Narration;

use App\Http\Controllers\Controller;
use App\Narration;
use App\SpeechOrbit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class SpeechOrbitController extends Controller
{
    public function create(Narration $narration)
    {
        $speech_orbits = SpeechOrbit::all();
        return view('dashboard.narrations.speech_orbits._create', compact('narration', 'speech_orbits'));

    }//end of create

    public function store(Request $request, Narration $narration)
    {
        if ($request->filled('name')) {

            $request->validate([
                'name' => 'required_without:speech_orbit_id|unique:speech_orbits',
            ]);

            $this->add_new_speech_orbit($request, $narration);

        } else {

            $request->validate([
                'speech_orbit_id' => ['required_without:name', Rule::unique('narration_speech_orbit')->where(function ($query) use ($narration) {
                    return $query->whereIn('speech_orbit_id', $narration->speech_orbits()->pluck('speech_orbit_id')->toArray());
                })],
            ]);

            $this->attach_speech_orbit($request, $narration);

        }//end of else

        session()->flash('success', __('site.added_successfully'));
        return view('dashboard.narrations.speech_orbits._index', compact('narration'));

    }//end of store

    public function sort(Request $request, Narration $narration)
    {
        foreach ($request->ids as $index => $id) {

            DB::table('narration_speech_orbit')->where('id', $id)->update(['order' => $index + 1]);

        }//end of for each

        session()->flash('success', __('site.sorted_successfully'));
        return view('dashboard.narrations.speech_orbits._index', compact('narration'));

    }//end of sort

    public function edit(Narration $narration, SpeechOrbit $speech_orbit)
    {
        $speech_orbits = SpeechOrbit::all();
        $narration_speech_orbit = $narration->speech_orbits()->where('speech_orbit_id', $speech_orbit->id)->first();

        return view('dashboard.narrations.speech_orbits._edit',
            compact('narration',
                'speech_orbits',
                'speech_orbit',
                'narration_speech_orbit'
            ));

        return view('dashboard.narrations.speech_orbits._edit', compact('narration', 'speech_orbits', 'speech_orbit'));

    }//end of edit

    public function update(Request $request, Narration $narration, SpeechOrbit $speech_orbit)
    {
        if ($request->filled('name')) {

            $request->validate([
                'name' => 'required_without:speech_orbit_id|unique:speech_orbits',
            ]);

            $this->add_new_speech_orbit($request, $narration, $speech_orbit);

        } else {

            $request->validate([
                'speech_orbit_id' => [
                    'required_without:name',
                    Rule::unique('narration_speech_orbit')->where(function ($query) use ($narration) {
                        return $query->whereIn('speech_orbit_id', $narration->speech_orbits()->pluck('speech_orbit_id')->toArray());
                    })->ignore($speech_orbit->id, 'speech_orbit_id')
                ]
            ]);

            $this->attach_speech_orbit($request, $narration, $speech_orbit);

        }//end of else

        session()->flash('success', __('site.updated_successfully'));
        return view('dashboard.narrations.speech_orbits._index', compact('narration'));

    }//end of update

    public function destroy(Narration $narration, SpeechOrbit $speech_orbit)
    {
        $narration->speech_orbits()->detach($speech_orbit->id);
        session()->flash('success', __('site.deleted_successfully'));
        return view('dashboard.narrations.speech_orbits._index', compact('narration'));

    }//end of destroy

    private function add_new_speech_orbit($request, $narration, $speech_orbit = null)
    {
        $new_speech_orbit = SpeechOrbit::create(['name' => $request->name]);

        if (!$speech_orbit) {

            $narration->speech_orbits()->attach($new_speech_orbit->id, [
                'notes' => $request->notes
            ]);

        } else {

            $narration->speech_orbits()->updateExistingPivot($speech_orbit->id, [
                'speech_orbit_id' => $new_speech_orbit->id,
                'notes' => $request->notes
            ]);

        }//end of else

    }//end of add new speech_orbit

    private function attach_speech_orbit($request, $narration, $speech_orbit = null)
    {
        if (!$speech_orbit) {

            $narration->speech_orbits()->attach($request->speech_orbit_id, [
                'notes' => $request->notes
            ]);

        } else {

            $narration->speech_orbits()->updateExistingPivot($speech_orbit->id, [
                'speech_orbit_id' => $request->speech_orbit_id,
                'notes' => $request->notes
            ]);

        }//end of else

    }//end of attach speech_orbit

}//end of controller
