<?php

namespace App\Http\Controllers\Dashboard\Narration;

use App\Http\Controllers\Controller;
use App\IndicatorBook;
use App\Narration;
use App\NarrationBookIndicatorPivot;
use App\NarrationIndicatorBookPivot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class IndicatorBookController extends Controller
{
    public function create(Narration $narration)
    {
        $indicator_books = IndicatorBook::all();
        return view('dashboard.narrations.indicator_books._create', compact('narration', 'indicator_books'));

    }//end of create

    public function store(Request $request, Narration $narration)
    {
        if ($request->filled('name')) {

            $request->validate([
                'name' => 'required_without:indicator_book_id|unique:indicator_books',
                'part' => 'required',
                'page' => 'required',
                'speech_number' => 'required',
            ]);

            $this->add_new_indicator_book($request, $narration);

        } else {

            $request->validate([
                'indicator_book_id' => ['required_without:name'],
                'part' => 'required',
                'page' => 'required',
                'speech_number' => ['required', Rule::unique('narration_indicator_book')->where(function ($q) use ($narration) {
                    return $q->whereIn('speech_number', $narration->indicator_books()->pluck('speech_number')->toArray())
                        ->whereIn('indicator_book_id', $narration->indicator_books()->pluck('narration_indicator_book.id')->toArray());
                })],
            ]);

            $this->attach_indicator_book($request, $narration);

        }//end of else

        session()->flash('success', __('site.added_successfully'));
        return view('dashboard.narrations.indicator_books._index', compact('narration'));

    }//end of store

    public function sort(Request $request, Narration $narration)
    {
        foreach ($request->ids as $index => $id) {

            DB::table('narration_indicator_book')->where('id', $id)->update(['order' => $index + 1]);

        }//end of for each

        session()->flash('success', __('site.sorted_successfully'));
        return view('dashboard.narrations.indicator_books._index', compact('narration'));

    }//end of sort

    public function edit(Request $request, Narration $narration, IndicatorBook $indicator_book)
    {
        $indicator_books = IndicatorBook::all();
        $narration_indicator_book = $narration->indicator_books()->where('narration_indicator_book.id', $request->pivot_id)->first();

        return view('dashboard.narrations.indicator_books._edit',
            compact('narration',
                'indicator_books',
                'indicator_book',
                'narration_indicator_book'
            ));

        return view('dashboard.narrations.indicator_books._edit', compact('narration', 'indicator_books', 'indicator_book'));

    }//end of edit

    public function update(Request $request, Narration $narration, IndicatorBook $indicator_book)
    {
        if ($request->filled('name')) {

            $request->validate([
                'name' => 'required_without:indicator_book_id|unique:indicator_books',
            ]);

            $this->add_new_indicator_book($request, $narration, $indicator_book);

        } else {

            $request->validate([
                'indicator_book_id' => [
                    'required_without:name',
                    //Rule::unique('narration_indicator_book')->where(function ($query) use ($narration) {
                    //    return $query->whereIn('indicator_book_id', $narration->indicator_books()->pluck('indicator_book_id')->toArray());
                    //})->ignore($indicator_book->id, 'indicator_book_id')
                ],
                'speech_number' => ['required', Rule::unique('narration_indicator_book')->where(function ($q) use ($narration) {
                    return $q->whereIn('speech_number', $narration->indicator_books()->pluck('speech_number')->toArray());
                })],
            ]);

            $this->attach_indicator_book($request, $narration, $indicator_book);

        }//end of else

        session()->flash('success', __('site.updated_successfully'));
        return view('dashboard.narrations.indicator_books._index', compact('narration'));

    }//end of update

    public function destroy(Request $request, Narration $narration, IndicatorBook $indicator_book)
    {
        $narration_indicator_book = NarrationIndicatorBookPivot::FindOrFail($request->pivot_id);
        $narration_indicator_book->delete();

        session()->flash('success', __('site.deleted_successfully'));
        return view('dashboard.narrations.indicator_books._index', compact('narration'));

    }//end of destroy

    private function add_new_indicator_book($request, $narration, $indicator_book = null)
    {
        $new_indicator_book = IndicatorBook::create(['name' => $request->name]);

        if (!$indicator_book) {

            $narration->indicator_books()->attach($new_indicator_book->id, [
                'part' => $request->part,
                'page' => $request->page,
                'speech_number' => $request->speech_number,
            ]);

        } else {

            $narration->indicator_books()->updateExistingPivot($indicator_book->id, [
                'indicator_book_id' => $new_indicator_book->id,
                'part' => $request->part,
                'page' => $request->page,
                'speech_number' => $request->speech_number,
            ]);

        }//end of else

    }//end of add new indicator_book

    private function attach_indicator_book($request, $narration, $indicator_book = null)
    {
        if (!$indicator_book) {

            $narration->indicator_books()->attach($request->indicator_book_id, [
                'part' => $request->part,
                'page' => $request->page,
                'speech_number' => $request->speech_number,
            ]);

        } else {

            $narration_indicator_book = NarrationIndicatorBookPivot::FindOrFail($request->pivot_id);
            $narration_indicator_book->update($request->except(['pivot_id', 'name']));

            //$narration->indicator_books()->updateExistingPivot($request->pivot_id, [
            //    'indicator_book_id' => $request->indicator_book_id,
            //    'part' => $request->part,
            //    'page' => $request->page,
            //    'speech_number' => $request->speech_number,
            //]);

        }//end of else

    }//end of attach indicator_book

}//end of controller
