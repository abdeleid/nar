<?php

namespace App\Http\Controllers\Dashboard\Narration;

use App\Category;
use App\Http\Controllers\Controller;
use App\Narration;
use App\NarrationField;
use Illuminate\Http\Request;

class NarrationController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::all();

        $narrations = Narration::select('narrations.id')
            ->join('speeches', 'narrations.id', 'speeches.narration_id')
            ->join('categories', 'speeches.category_id', 'categories.id')
            ->join('category_narration_field', 'categories.id', 'category_narration_field.category_id')
            ->join('narration_fields', 'category_narration_field.narration_field_id', 'narration_fields.id')
            ->when($request->category_id, function ($q) use ($request) {
                return $q->where('categories.id', $request->category_id);
            })
            ->when($request->speech_number, function ($q) use ($request) {
                return $q->where('speeches.speech_number', $request->speech_number);
            })
            ->when($request->secondary_number, function ($q) use ($request) {
                return $q->where('speeches.secondary_number', $request->secondary_number);
            })
            ->groupBy('narrations.id')
            ->paginate(15);

        return response(view('dashboard.narrations.index', compact('categories', 'narrations')))->cookie('category_id', $request->category_id);

    }//end of index

    public function create()
    {
        $narration = auth()->user()->narrations()->create();
        $narration_fields = NarrationField::all();
        return view('dashboard.narrations.create', compact('narration', 'narration_fields'));

    }//end of create

    public function edit(Narration $narration)
    {
        $narration_fields = NarrationField::all();
        return view('dashboard.narrations.edit', compact('narration', 'narration_fields'));

    }//end of edit

    public function update(Request $request, Narration $narration)
    {
        $narration->update($request->all());
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.narrations.index');

    }//end of update

    public function destroy(Narration $narration)
    {
        $narration->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.narrations.index');

    }//end of destroy

}//end of controller
