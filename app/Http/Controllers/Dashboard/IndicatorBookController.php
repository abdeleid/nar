<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\IndicatorBook;
use Illuminate\Http\Request;

class IndicatorBookController extends Controller
{
    public function index(Request $request)
    {
        $indicator_books = IndicatorBook::when($request->search, function ($q) use ($request) {

            return $q->where('name', 'like', '%' . $request->search . '%');

        })->latest()->paginate(5);

        return view('dashboard.indicator_books.index', compact('indicator_books'));

    }//end of index

    public function create()
    {
        return view('dashboard.indicator_books.create');

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        IndicatorBook::create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.indicator_books.index');

    }//end of store

    public function edit(IndicatorBook $indicator_book)
    {
        return view('dashboard.indicator_books.edit', compact('indicator_book'));

    }//end of edit

    public function update(Request $request, IndicatorBook $indicator_book)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $indicator_book->update($request->all());
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.indicator_books.index');

    }//end of update

    public function destroy(IndicatorBook $indicator_book)
    {
        $indicator_book->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.indicator_books.index');

    }//end of destroy

}//end of controller
