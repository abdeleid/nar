<?php

namespace App\Http\Controllers\Dashboard\Category;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MissingSpeechController extends Controller
{
    public function index(Request $request, Category $category)
    {
        $number_of_speeches = collect(range(1, $category->number_of_speeches));
        $speech_numbers = $category->speeches()->pluck('speech_number');

        $missing_speeches = $number_of_speeches->diff($speech_numbers);

        return view('dashboard.categories.missing_speeches.index', compact('missing_speeches'));

    }//end of index

}//end of controller
