<?php

namespace App\Http\Controllers\Dashboard\Category;

use App\Category;
use App\Http\Controllers\Controller;
use App\NarrationField;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::orderBy('order', 'asc')->get();
        return view('dashboard.categories.index', compact('categories'));

    }//end of index

    public function create()
    {
        $narration_fields = NarrationField::all();
        return view('dashboard.categories.create', compact('narration_fields'));

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'number_of_speeches' => 'required',
            'narration_fields' => 'required|array|min:1',
        ]);

        $request->merge(['order' => Category::count() + 1]);
        $category = Category::create($request->except(['narration_fields']));
        $category->narration_fields()->attach($request->narration_fields);

        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.categories.index');

    }//end of store

    public function sort(Request $request)
    {
        foreach ($request->ids as $index => $id) {

            $category = Category::FindOrFail($id);
            $category->update(['order' => $index + 1]);

        }//end of for each

        return response()->json([
            'message' => __('site.sorted_successfully')
        ]);

    }//end of sort

    public function edit(Category $category)
    {
        $narration_fields = NarrationField::all();
        return view('dashboard.categories.edit', compact('category', 'narration_fields'));

    }//end of edit

    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name' => 'required',
            'number_of_speeches' => 'required',
            'narration_fields' => 'required|array|min:1',
        ]);

        $category->update($request->except(['narration_fields']));
        $category->narration_fields()->sync($request->narration_fields);

        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.categories.index');

    }//end of update

    public function destroy(Category $category)
    {
        $category->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.categories.index');

    }//end of destroy

}//end of controller
