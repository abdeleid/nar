<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Provision;
use Illuminate\Http\Request;

class ProvisionController extends Controller
{
    public function index(Request $request)
    {
        $provisions = Provision::when($request->search, function ($q) use ($request) {

            return $q->where('name', 'like', '%' . $request->search . '%');

        })->latest()->paginate(5);
        
        return view('dashboard.provisions.index', compact('provisions'));

    }//end of index

    public function create()
    {
        return view('dashboard.provisions.create');

    }//end of create

    public function store(Request $request, Provision $provision)
    {
        $request->validate([
            'name' => 'required|unique:provisions,name',
        ]);

        Provision::create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.provisions.index');

    }//end of store

    public function edit(Provision $provision)
    {
        return view('dashboard.provisions.edit', compact('provision'));

    }//end of edit

    public function update(Request $request, Provision $provision)
    {
        $request->validate([
            'name' => 'required|unique:provisions,name,' . $provision->id,
        ]);

        $provision->update($request->all());
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.provisions.index');

    }//end of update

    public function destroy(Provision $provision)
    {
        $provision->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.provisions.index');

    }//end of destroy

}//end of controller
