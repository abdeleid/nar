<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\NarrationField;
use Illuminate\Http\Request;

class NarrationFieldController extends Controller
{
    public function index(Request $request)
    {
        $narration_fields = NarrationField::when($request->search, function ($q) use ($request) {

            return $q->where('name', 'like', '%' . $request->search . '%');

        })->latest()->paginate(5);

        return view('dashboard.narration_fields.index', compact('narration_fields'));

    }//end of index

    public function create()
    {
        return view('dashboard.narration_fields.create');

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        NarrationField::create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.narration_fields.index');

    }//end of store

    public function edit(NarrationField $narration_field)
    {
        return view('dashboard.narration_fields.edit', compact('narration_field'));

    }//end of edit

    public function update(Request $request, NarrationField $narration_field)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $narration_field->update($request->all());
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.narration_fields.index');

    }//end of update

    public function destroy(NarrationField $narration_field)
    {
        $narration_field->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.narration_fields.index');

    }//end of destroy

}//end of controller
