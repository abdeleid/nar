<?php

namespace App\Http\Controllers\Dashboard;

use App\BookReference;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BookReferenceController extends Controller
{
    public function index(Request $request)
    {
        $book_references = BookReference::when($request->search, function ($q) use ($request) {

            return $q->where('name', 'like', '%' . $request->search . '%');
            
        })->latest()->paginate(5);

        return view('dashboard.book_references.index', compact('book_references'));

    }//end of index

    public function create()
    {
        return view('dashboard.book_references.create');

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        BookReference::create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.book_references.index');

    }//end of store

    public function edit(BookReference $book_reference)
    {
        return view('dashboard.book_references.edit', compact('book_reference'));

    }//end of edit

    public function update(Request $request, BookReference $book_reference)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $book_reference->update($request->all());
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.book_references.index');

    }//end of update

    public function destroy(BookReference $book_reference)
    {
        $book_reference->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.book_references.index');

    }//end of destroy

}//end of controller
