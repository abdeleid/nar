<?php

namespace App\Http\Controllers\Dashboard;

use App\Classification;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ClassificationController extends Controller
{
    public function index(Request $request)
    {
        $type = $request->type;
        $classifications = Classification::where('type', $type)
            ->when($request->search, function ($q) use ($request) {

                return $q->where('name', 'like', '%' . $request->search . '%');
            })
            ->latest()
            ->paginate(5);

        return view('dashboard.classifications.index', compact('classifications', 'type'));

    }//end of index

    public function create(Request $request)
    {
        $type = $request->type;
        return view('dashboard.classifications.create', compact('type'));

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'type' => 'required|in:alerts',
        ]);


        $classification = Classification::create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.classifications.index', ['type' => $classification->type]);

    }//end of store

    public function edit(Classification $classification)
    {
        return view('dashboard.classifications.edit', compact('classification'));

    }//end of edit

    public function update(Request $request, Classification $classification)
    {
        $request->validate([
            'name' => 'required',
            'type' => 'required|in:alerts',
        ]);

        $classification->update($request->all());
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.classifications.index', ['type' => $classification->type]);

    }//end of update

    public function destroy(Classification $classification)
    {
        $type = $classification->type;
        $classification->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.classifications.index', ['type' => $type]);

    }//end of destroy

}//end of controller
