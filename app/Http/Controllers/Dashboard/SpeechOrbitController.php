<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\SpeechOrbit;
use Illuminate\Http\Request;

class SpeechOrbitController extends Controller
{
    public function index(Request $request)
    {
        $speech_orbits = SpeechOrbit::when($request->search, function ($q) use ($request) {

            return $q->where('name', 'like', '%' . $request->search . '%');

        })->latest()->paginate(5);

        return view('dashboard.speech_orbits.index', compact('speech_orbits'));

    }//end of index

    public function create()
    {
        return view('dashboard.speech_orbits.create');

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        SpeechOrbit::create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.speech_orbits.index');

    }//end of store

    public function edit(SpeechOrbit $speech_orbit)
    {
        return view('dashboard.speech_orbits.edit', compact('speech_orbit'));

    }//end of edit

    public function update(Request $request, SpeechOrbit $speech_orbit)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $speech_orbit->update($request->all());
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.speech_orbits.index');

    }//end of update

    public function destroy(SpeechOrbit $speech_orbit)
    {
        $speech_orbit->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.speech_orbits.index');

    }//end of destroy

}//end of controller
