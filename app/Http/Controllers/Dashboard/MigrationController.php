<?php

  namespace App\Http\Controllers\Dashboard;

  use App\Traits\HasMigration;
  use Illuminate\Http\Request;
  use App\Http\Controllers\Controller;

  class MigrationController extends Controller
  {
      use HasMigration;

      public function __invoke()
      {
          $this->migrate_users($this->users);
          $this->migrate_categories($this->categories);
          $this->migrate_narrations($this->narrations);
          $this->migrate_speeches($this->speeches);


          return "migration done successfully";

      }//end of invoke

  }//end of controller
