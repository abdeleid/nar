<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Scientist;
use Illuminate\Http\Request;

class ScientistController extends Controller
{
    public function index(Request $request)
    {
        $scientists = Scientist::when($request->search, function ($q) use ($request) {

            return $q->where('name', 'like', '%' . $request->search . '%');
            
        })->latest()->paginate(5);

        return view('dashboard.scientists.index', compact('scientists'));

    }//end of index

    public function create()
    {
        return view('dashboard.scientists.create');

    }//end of create

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
        ]);

        Scientist::create($request->all());
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.scientists.index');

    }//end of store

    public function edit(Scientist $scientist)
    {
        return view('dashboard.scientists.edit', compact('scientist'));

    }//end of edit

    public function update(Request $request, Scientist $scientist)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $scientist->update($request->all());
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.scientists.index');

    }//end of update

    public function destroy(Scientist $scientist)
    {
        $scientist->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.scientists.index');

    }//end of destroy

}//end of model
