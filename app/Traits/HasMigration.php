<?php

namespace App\Traits;

use App\Category;
use App\Narration;
use App\Speech;
use App\User;

trait HasMigration{

    public function migrate_users($old_users)
    {
        foreach ($old_users as $old_user) {

            $user = User::create([
                'id' => $old_user['id'],
                'name' => $old_user['name'],
                'email' => $old_user['email'],
                'password' => $old_user['password'],
            ]);

            $user->attachRole('super_admin');

        }//end of for each

    }//end of migrate users

    public function migrate_categories($old_categories)
    {
        foreach ($old_categories as $old_category) {

            Category::create([
                'id' => $old_category['id'],
                'parent' => $old_category['parent'],
                'name' => $old_category['title'],
                'type' => $old_category['type'],
                'order' => $old_category['order'],
                'number_of_speeches' => $old_category['number'],
            ]);

        }//end of for each

    }//end of migrate categories

    public function migrate_narrations($old_narrations)
    {
        foreach ($old_narrations as $old_narration) {

            Narration::create([
                'id' => $old_narration['id'],
                'user_id' => $old_narration['user_id'],
            ]);

        }//end of for each

    }//end of migrate old narrations

    public function migrate_speeches($old_speeches)
    {
        foreach ($old_speeches as $old_speech) {

            Speech::create([
                'id' => $old_speech['id'],
                'narration_id' => $old_speech['takhregat_id'],
                'category_id' => $old_speech['category_id'],
                'speech_number' => $old_speech['number'],
                'secondary_number' => $old_speech['subnumber'],
                'order' => $old_speech['order'],
            ]);

        }//end of for each

    }//end of migrate speeches

}//end of trait