<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNarrationSpeechOrbitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('narration_speech_orbit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('narration_id')->unsigned();
            $table->bigInteger('speech_orbit_id')->unsigned();
            $table->integer('order')->default(0);
            $table->text('notes')->nullable();

            $table->foreign('narration_id')->references('id')->on('narrations')->onDelete('cascade');
            $table->foreign('speech_orbit_id')->references('id')->on('speech_orbits')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('narration_speech_orbit');
    }
}
