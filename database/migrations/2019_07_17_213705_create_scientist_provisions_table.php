<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScientistProvisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scientist_provisions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('narration_id')->unsigned()->nullable();
            $table->bigInteger('scientist_id')->unsigned()->nullable();
            $table->bigInteger('provision_id')->unsigned()->nullable();
            $table->bigInteger('book_reference_id')->unsigned()->nullable();
            $table->integer('book_reference_part')->nullable();
            $table->integer('book_reference_page')->nullable();
            $table->integer('order')->default(0);
            $table->text('notes')->nullable();
            $table->timestamps();
            
            $table->foreign('narration_id')->references('id')->on('narrations')->onDelete('cascade');
            $table->foreign('scientist_id')->references('id')->on('scientists')->onDelete('cascade');
            $table->foreign('provision_id')->references('id')->on('provisions')->onDelete('cascade');
            $table->foreign('book_reference_id')->references('id')->on('book_references')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scientist_provisions');
    }
}
