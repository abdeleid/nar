<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNarrationIndicatorBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('narration_indicator_book', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('narration_id')->unsigned();
            $table->bigInteger('indicator_book_id')->unsigned();
            $table->integer('order')->default(0);
//            $table->text('numbers');
            $table->string('part');
            $table->string('page');
            $table->string('speech_number');

            $table->foreign('narration_id')->references('id')->on('narrations')->onDelete('cascade');
            $table->foreign('indicator_book_id')->references('id')->on('indicator_books')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('narration_indicator_book');
    }
}
