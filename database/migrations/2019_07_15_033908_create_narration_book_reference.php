<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNarrationBookReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('narration_book_reference', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('narration_id')->unsigned();
            $table->bigInteger('book_reference_id')->unsigned();
            $table->integer('order')->default(0);
            $table->integer('part');
            $table->integer('page');
            
            $table->foreign('narration_id')->references('id')->on('narrations')->onDelete('cascade');
            $table->foreign('book_reference_id')->references('id')->on('book_references')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('narration_book_reference');
    }
}
