<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPartNumberAndPageNumberToSpeeches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('speeches', function (Blueprint $table) {
            $table->integer('part_number')->after('speech_number')->nullable();
            $table->integer('page_number')->after('speech_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('speeches', function (Blueprint $table) {
            $table->dropColumn('part_number');
            $table->dropColumn('page_number');
        });
    }
}
