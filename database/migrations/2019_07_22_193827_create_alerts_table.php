<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alerts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('narration_id')->unsigned();
            $table->bigInteger('classification_id')->unsigned()->nullable();
            $table->string('alert')->nullable();
            $table->integer('order')->default(0);
            $table->bigInteger('book_reference_id')->unsigned()->nullable();
            $table->string('book_reference_part')->nullable();
            $table->string('book_reference_page')->nullable();
            $table->text('det')->nullable();

            $table->foreign('narration_id')->references('id')->on('narrations')->onDelete('cascade');
            $table->foreign('classification_id')->references('id')->on('classifications')->onDelete('cascade');
            $table->foreign('book_reference_id')->references('id')->on('book_references')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alerts');
    }
}
