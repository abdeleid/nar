<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpeechesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('speeches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('narration_id')->unsigned();
            $table->bigInteger('category_id')->unsigned();
            $table->integer('order')->default(0);
            $table->string('speech_number')->nullable();
            $table->string('secondary_number')->nullable();
            $table->timestamps();

            $table->foreign('narration_id')->references('id')->on('narrations')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('speeches');
    }
}
