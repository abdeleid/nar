<div class="page-sidebar">

    {{--site header--}}
    <header class="site-header">
        <div class="site-logo"><a href="{{ route('dashboard.welcome') }}">موسوعه التخريج</a></div>
        <div class="sidebar-collapse hidden-xs"><a class="sidebar-collapse-icon" href="#"><i class="icon-menu"></i></a></div>
        <div class="sidebar-mobile-menu visible-xs"><a data-target="#side-nav" data-toggle="collapse" class="mobile-menu-icon" href="#"><i class="icon-menu"></i></a></div>
    </header>

    {{--main navigation--}}
    <ul id="side-nav" class="main-menu navbar-collapse collapse">

        <li><a href="{{ route('dashboard.welcome') }}"><i class="fa fa-magnet"></i><span class="title">@lang('site.welcome')</span></a></li>

        @if (auth()->user()->hasPermission('read_classifications'))

            <li class="has-sub"><a href=""><i class="icon-gauge"></i><span class="title">@lang('site.classifications')</span></a>
                <ul class="nav collapse">
                    <li><a href="{{ route('dashboard.classifications.index', ['type' => 'alerts']) }}"><span class="title">@lang('site.alerts')</span></a></li>
                </ul>
            </li>

        @endif

        @if (auth()->user()->hasPermission('read_narration_fields'))
            <li><a href="{{ route('dashboard.narration_fields.index') }}"><i class="fa fa-magnet"></i><span class="title">@lang('site.narration_fields')</span></a></li>
        @endif

        @if (auth()->user()->hasPermission('read_categories'))
            <li><a href="{{ route('dashboard.categories.index') }}"><i class="fa fa-magnet"></i><span class="title">@lang('site.categories')</span></a></li>
        @endif

        @if (auth()->user()->hasPermission('read_scientists'))
            <li><a href="{{ route('dashboard.scientists.index') }}"><i class="fa fa-magnet"></i><span class="title">@lang('site.scientists')</span></a></li>
        @endif

        @if (auth()->user()->hasPermission('read_book_references'))
            <li><a href="{{ route('dashboard.book_references.index') }}"><i class="fa fa-magnet"></i><span class="title">@lang('site.book_references')</span></a></li>
        @endif

        @if (auth()->user()->hasPermission('read_provisions'))
            <li><a href="{{ route('dashboard.provisions.index') }}"><i class="fa fa-magnet"></i><span class="title">@lang('site.provisions')</span></a></li>
        @endif

        @if (auth()->user()->hasPermission('read_indicator_books'))
            <li><a href="{{ route('dashboard.indicator_books.index') }}"><i class="fa fa-magnet"></i><span class="title">@lang('site.indicator_books')</span></a></li>
        @endif

        @if (auth()->user()->hasPermission('read_speech_orbits'))
            <li><a href="{{ route('dashboard.speech_orbits.index') }}"><i class="fa fa-magnet"></i><span class="title">@lang('site.speech_orbits')</span></a></li>
        @endif

        @if (auth()->user()->hasPermission('read_narrations'))
            <li><a href="{{ route('dashboard.narrations.index', ['category_id' => request()->cookie('category_id')]) }}"><i class="fa fa-magnet"></i><span class="title">@lang('site.narrations')</span></a></li>
        @endif

        @if (auth()->user()->hasPermission('read_users'))
            <li><a href="{{ route('dashboard.users.index') }}"><i class="fa fa-magnet"></i><span class="title">@lang('site.users')</span></a></li>
        @endif

        {{--<li><a href="{{ route('dashboard.videos.index') }}"><i class="fa fa-magnet"></i><span class="title">@lang('site.videos')</span></a></li>--}}

        {{--<li class="has-sub"><a href="index.html"><i class="icon-gauge"></i><span class="title">Dashboard</span></a>--}}
        {{--<ul class="nav collapse">--}}
        {{--<li><a href="index.html"><span class="title">Misc.</span></a></li>--}}
        {{--<li><a href="ecommerce-dashboard.html"><span class="title">E-Commerce</span></a></li>--}}
        {{--<li><a href="news-dashboard.html"><span class="title">News Portal</span></a></li>--}}
        {{--</ul>--}}
        {{--</li>--}}

    </ul><!-- end of main menu -->

</div><!-- end of page side bar -->
