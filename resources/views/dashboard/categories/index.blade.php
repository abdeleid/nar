@extends('layouts.dashboard.app')

@section('content')

    <h1 class="page-title">@lang('site.categories')</h1>

    {{--breadcrumb--}}
    <ol class="breadcrumb breadcrumb-2">
        <li><a href="{{ route('dashboard.welcome') }}"><i class="fa fa-home"></i>@lang('site.welcome')</a></li>
        <li class="active"><strong>@lang('site.categories')</strong></li>
    </ol>

    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default">

                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-12">
                            <a href="{{ route('dashboard.categories.create') }}" class="btn btn-primary" style="margin-bottom: 2rem;"><i class="fa fa-plus"></i> @lang('site.add')</a>
                        </div><!-- end of col -->

                    </div><!-- end of row -->

                    <div class="row" id="category-list" data-url="{{ route('dashboard.categories.sort') }}">

                        @forelse ($categories as $category)

                            <div class="col-md-3 category" id="{{ $category->id }}">

                                <div class="well" style="background: white; cursor:move;">

                                    <div style="display: flex;">
                                        <span class="drag-icon" style="margin-left: 1rem;"><i class="fa fa-ellipsis-v"></i></span>
                                        <p>{{ $category->name }}</p>
                                    </div>
                                    <a href="{{ route('dashboard.categories.edit', $category->id) }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> @lang('site.edit')</a>
                                    <form action="{{ route('dashboard.categories.destroy', $category->id) }}" style="display: inline-block;" method="post">
                                        {{ csrf_field() }}
                                        {{ method_field('delete') }}
                                        <button class="btn btn-danger btn-sm delete" type="submit"><i class="fa fa-trash"></i> @lang('site.delete')</button>

                                    </form><!-- end of form -->
                                    <a href="{{ route('dashboard.categories.missing_speeches.index', $category->id) }}" class="btn btn-warning btn-sm"><i class="fa fa-minus"></i> @lang('site.missing_speeches')</a>

                                </div><!-- end of well -->

                            </div><!-- end of col -->

                        @empty

                            <div class="col-md-12">
                                <h2>@lang('site.no_data_found')</h2>
                            </div>

                        @endforelse

                    </div><!-- end of row -->

                </div><!-- end of panel body -->

            </div><!-- end of panel default -->

        </div><!-- end of col -->

    </div><!-- end of row -->
@endsection

@push('scripts')

    <script>
        $(document).ready(function () {

            $('#category-list').sortable({
                items: "> .category",
                cursor: "move",
                tolerance: 'pointer',
                // dropOnEmpty: false,
                // containment: "parent",
                helper: 'clone',
                opacity: 0.5,
                revert: 50,
                forceHelperSize: true,

                update: function (event, ui) {

                    let url = $(this).data('url');
                    let ids = $(this).sortable('toArray');

                    let data = {
                        '_token': $('meta[name="csrf-token"]').attr('content'),
                        'ids': ids
                    }

                    $.ajax({
                        url: url,
                        method: 'post',
                        data: data,
                        success: function (data) {

                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: data.message,
                                timeout: 2000,
                                killer: true
                            }).show();

                        },
                    })
                }
            }).disableSelection();

        });//end of document ready

    </script>

@endpush