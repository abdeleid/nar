@extends('layouts.dashboard.app')

@section('content')

    <h1 class="page-title">@lang('site.categories')</h1>

    {{--breadcrumb--}}
    <ol class="breadcrumb breadcrumb-2">
        <li><a href="{{ route('dashboard.welcome') }}"><i class="fa fa-home"></i>@lang('site.welcome')</a></li>
        <li><a href="{{ route('dashboard.categories.index') }}">@lang('site.categories')</a></li>
        <li class="active"><strong>@lang('site.edit')</strong></li>
    </ol>


    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default">

                <div class="panel-body">

                    <form action="{{ route('dashboard.categories.update', $category->id) }}" method="post">

                        {{ csrf_field() }}
                        {{ method_field('put') }}

                        @include('dashboard.partials._errors')

                        {{--name--}}
                        <div class="form-group">
                            <label>@lang('site.name')</label>
                            <input type="text" name="name" class="form-control" value="{{ $category->name ?? old('name') }}">
                        </div>

                        {{--number of speeches--}}
                        <div class="form-group">
                            <label>@lang('site.number_of_speeches')</label>
                            <input type="number" name="number_of_speeches" class="form-control" value="{{ $category->number_of_speeches ?? old('number_of_speeches') }}">
                        </div>

                        {{--narration fields--}}
                        <div class="form-group">
                            <label>@lang('site.narration_fields')</label>
                            <select name="narration_fields[]" class="form-control select2" multiple>
                                @foreach ($narration_fields as $narration_field)
                                    <option value="{{ $narration_field->id }}" {{ $category->narration_fields->pluck('id')->contains($narration_field->id) ? 'selected' : '' }}>{{ $narration_field->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i> @lang('site.edit')</button>
                        </div>

                    </form><!-- end of form -->

                </div><!-- end of panel body -->

            </div><!-- end of panel default -->

        </div><!-- end of col -->

    </div><!-- end of row -->
@endsection
