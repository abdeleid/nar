@extends('layouts.dashboard.app')

@section('content')

    <h1 class="page-title">@lang('site.categories')</h1>

    {{--breadcrumb--}}
    <ol class="breadcrumb breadcrumb-2">
        <li><a href="{{ route('dashboard.welcome') }}"><i class="fa fa-home"></i>@lang('site.welcome')</a></li>
        <li><a href="{{ route('dashboard.categories.index') }}">@lang('site.categories')</a></li>
        <li class="active"><strong>@lang('site.missing_speeches')</strong></li>
    </ol>

    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default">

                <div class="panel-body">


                    @foreach ($missing_speeches as $index => $missing_speech)
                    
                        <p>{{ $missing_speech }}</p>
                        
                    @endforeach

                </div><!-- end of panel body -->

            </div><!-- end of panel default -->

        </div><!-- end of col -->

    </div><!-- end of row -->
@endsection

@push('scripts')

    <script>
        $(document).ready(function () {

            $('#category-list').sortable({
                items: "> .category",
                cursor: "move",
                tolerance: 'pointer',
                // dropOnEmpty: false,
                // containment: "parent",
                helper: 'clone',
                opacity: 0.5,
                revert: 50,
                forceHelperSize: true,

                update: function (event, ui) {

                    let url = $(this).data('url');
                    let ids = $(this).sortable('toArray');

                    let data = {
                        '_token': $('meta[name="csrf-token"]').attr('content'),
                        'ids': ids
                    }

                    $.ajax({
                        url: url,
                        method: 'post',
                        data: data,
                        success: function (data) {

                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                text: data.message,
                                timeout: 2000,
                                killer: true
                            }).show();

                        },
                    })
                }
            }).disableSelection();

        });//end of document ready

    </script>

@endpush