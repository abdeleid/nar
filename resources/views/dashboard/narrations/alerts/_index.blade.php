@include('dashboard.partials._session')

@if ($narration->alerts)

    <table class="table table-bordered sort" data-url="{{ route('dashboard.narrations.alerts.sort', $narration->id) }}" data-property="alerts" id="alerts-table">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('site.classification')</th>
            <th>@lang('site.title')</th>
            <th>@lang('site.det')</th>
            <th>@lang('site.edit')</th>
            <th>@lang('site.delete')</th>
        </tr>
        </thead>

        <tbody>

        @foreach ($narration->alerts as $index => $alert)
            <tr id="{{ $alert->id }}" style="cursor: move;">
                <td>{{ $index + 1 }}</td>
                <td>{{ $alert->classification->name }}</td>
                <td>{{ $alert->alert }}</td>
                <td>{!! $alert->det !!}</td>
                <td>
                    <button class="btn btn-info btn-sm edit-narration-property"
                            data-url="{{ route('dashboard.narrations.alerts.edit', ['narration' => $narration->id, 'alert' => $alert->id]) }}"
                            data-title="@lang('site.edit')"
                            style="margin-bottom: 0;"
                    >
                        <i class="fa fa-edit"></i> @lang('site.edit')
                    </button>
                </td>
                <td>
                    <form action="{{ route('dashboard.narrations.alerts.destroy', ['narration' => $narration->id, 'alert' => $alert->id]) }}" method="post" class="narration-property-form" data-property="alerts" style="margin-bottom: 0;">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <button type="submit" class="btn btn-danger btn-sm delete" style="margin-bottom: 0;"><i class="fa fa-trash"></i> @lang('site.delete')</button>
                    </form>
                </td>
            </tr>
        @endforeach

        </tbody>

    </table><!-- end of table -->

    <script>
        $('#alerts-table tbody').sortable({
            tolerance: 'pointer',
            axis: "y",
            opacity: 0.50,
            cursor: "move",
            helper: "clone",

            update: function (event, ui) {

                let url = $(this).parent('table').data('url');
                let property = $(this).parent('table').data('property');
                let ids = $(this).sortable('toArray');
                let data = {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'ids': ids
                }

                $.ajax({
                    url: url,
                    method: 'post',
                    data: data,
                    success: function (data) {
                        $('#' + property + ' .accordion-inner').empty().append(data);
                    },
                })
            }
        })
    </script>

@endif