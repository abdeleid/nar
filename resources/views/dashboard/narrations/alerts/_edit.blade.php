<form action="{{ route('dashboard.narrations.alerts.update', ['narration' => $narration->id, 'alert' => $alert->id]) }}" class="narration-property-form" data-property="alerts" method="post">

    {{ csrf_field() }}
    {{ method_field('put') }}

    <div class="tabs-container">

        <ul class="nav nav-tabs">
            <li class="active"><a aria-expanded="true" href="#alert" data-toggle="tab">@lang('site.alert')</a></li>
            <li><a aria-expanded="false" href="#ref" data-toggle="tab">@lang('site.ref')</a></li>
            <li><a aria-expanded="false" href="#det" data-toggle="tab">@lang('site.det')</a></li>
        </ul>

        <div class="tab-content">

            <div class="tab-pane active" id="alert">

                <div class="panel-body">

                    {{--classification--}}
                    <div class="form-group">
                        <label>@lang('site.classification')</label>
                        <select name="classification_id" class="form-control select2">
                            <option value="" disabled selected>@lang('site.select')</option>
                            @foreach ($classifications as $classification)
                                <option value="{{ $classification->id }}" {{ $classification->id == $alert->classification_id ? 'selected' : '' }}>{{ $classification->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    {{--new name--}}
                    <div class="form-group">
                        <label>@lang('site.add_new')</label>
                        <input type="text" name="name" class="form-control">
                        <input type="hidden" name="type" value="alerts">
                    </div>

                    {{--alert--}}
                    <div class="form-group">
                        <label>@lang('site.alert')</label>
                        <input type="text" name="alert" class="form-control" value="{{ $alert->alert }}">
                    </div>

                </div><!-- end of panel body -->

            </div><!-- end of tab pane-->

            <div class="tab-pane" id="ref">

                <div class="panel-body">

                    {{--book reference id--}}
                    <div class="form-group">
                        <label>@lang('site.book_reference')</label>
                        <select name="book_reference_id" class="form-control select2">
                            <option value="" disabled selected>@lang('site.select')</option>
                            @foreach ($book_references as $book_reference)
                                <option value="{{ $book_reference->id }}" {{ $alert->book_reference_id == $book_reference->id ? 'selected' : '' }}>{{ $book_reference->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    {{--new book reference--}}
                    <div class="form-group">
                        <label>@lang('site.add_new')</label>
                        <input type="text" name="new_book_reference" class="form-control">
                    </div>

                    {{--part--}}
                    <div class="form-group">
                        <label>@lang('site.part')</label>
                        <input type="text" name="part" class="form-control" value="{{ $alert->book_reference_part }}">
                    </div>

                    {{--page--}}
                    <div class="form-group">
                        <label>@lang('site.page')</label>
                        <input type="text" name="page" class="form-control" value="{{ $alert->book_reference_page }}">
                    </div>
                    
                </div><!-- end of panel body -->

            </div><!-- end of tab pane -->

            <div class="tab-pane" id="det">

                <div class="panel-body">

                    {{--det--}}
                    <div class="form-group">
                        <label>@lang('site.det')</label>
                        <textarea name="det" class="form-control editable">{{ $alert->ref }}</textarea>
                    </div>

                </div><!-- end of panel body -->

            </div><!-- end of tab pane -->

        </div><!-- end of tab content -->

    </div><!-- end of tabs -->

    <div class="form-group">
        <input type="submit" value="@lang('site.edit')" class="btn btn-primary">
    </div>

</form><!-- end of form -->