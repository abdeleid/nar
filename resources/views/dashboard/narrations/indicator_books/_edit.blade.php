<form action="{{ route('dashboard.narrations.indicator_books.update', ['narration' => $narration->id, 'indicator_book' => $indicator_book->id]) }}" class="narration-property-form" data-property="indicator_books" method="post">

    {{ csrf_field() }}
    {{ method_field('put') }}

    <div class="form-group">
        <label>@lang('site.indicator_books')</label>
        <select name="indicator_book_id" class="form-control select2">
            <option value="" disabled>@lang('site.select')</option>
            @foreach ($indicator_books as $indicator_book)
                <option value="{{ $indicator_book->id }}" {{ $indicator_book->id == $narration_indicator_book->id ? 'selected' : '' }}>{{ $indicator_book->name }}</option>
            @endforeach
        </select>
    </div>

    <input type="hidden" name="pivot_id" value="{{ $narration_indicator_book->pivot->id }}">

    <div class="form-group">
        <label>@lang('site.add_new')</label>
        <input type="text" name="name" class="form-control">
    </div>

    {{--<div class="form-group">--}}
    {{--<label style="display: block;">@lang('site.numbers')</label>--}}
    {{--<input type="text" name="numbers" class="form-control taggable" value="{{ $narration_indicator_book->pivot->numbers }}">--}}
    {{--</div>--}}

    <div class="form-group">
        <label>@lang('site.part')</label>
        <input type="text" name="part" class="form-control" value="{{ $narration_indicator_book->pivot->part }}">
    </div>

    <div class="form-group">
        <label>@lang('site.page')</label>
        <input type="text" name="page" class="form-control" value="{{ $narration_indicator_book->pivot->page }}">
    </div>

    <div class="form-group">
        <label>@lang('site.speech_number')</label>
        <input type="text" name="speech_number" class="form-control" value="{{ $narration_indicator_book->pivot->speech_number }}">
    </div>

    <div class="form-group">
        <input type="submit" value="@lang('site.edit')" class="btn btn-primary">
    </div>

</form><!-- end of form -->