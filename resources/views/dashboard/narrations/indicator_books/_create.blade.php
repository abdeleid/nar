<form action="{{ route('dashboard.narrations.indicator_books.store', $narration->id) }}" class="narration-property-form" data-property="indicator_books" method="post">

    {{ csrf_field() }}
    {{ method_field('post') }}

    {{--indicator book id--}}
    <div class="form-group">
        <label>@lang('site.indicator_books')</label>
        <select name="indicator_book_id" class="form-control select2">
            <option value="" disabled selected>@lang('site.select')</option>
            @foreach ($indicator_books as $indicator_book)
                <option value="{{ $indicator_book->id }}">{{ $indicator_book->name }}</option>
            @endforeach
        </select>
    </div>

    {{--name--}}
    <div class="form-group">
        <label>@lang('site.add_new')</label>
        <input type="text" name="name" class="form-control">
    </div>

    {{--<div class="form-group">--}}
        {{--<label style="display: block;">@lang('site.numbers')</label>--}}
        {{--<input type="text" name="numbers" class="form-control taggable">--}}
    {{--</div>--}}

    <div class="form-group">
        <label>@lang('site.part')</label>
        <input type="text" name="part" class="form-control">
    </div>

    <div class="form-group">
        <label>@lang('site.page')</label>
        <input type="text" name="page" class="form-control">
    </div>

    <div class="form-group">
        <label>@lang('site.speech_number')</label>
        <input type="text" name="speech_number" class="form-control">
    </div>

    <div class="form-group">
        <input type="submit" value="@lang('site.add')" class="btn btn-primary">
    </div>

</form><!-- end of form -->