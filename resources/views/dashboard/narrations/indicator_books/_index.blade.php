@include('dashboard.partials._session')

@if ($narration->indicator_books)

    <table class="table table-bordered sort" data-url="{{ route('dashboard.narrations.indicator_books.sort', $narration->id) }}" data-property="indicator_books" id="indicator_books-table">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('site.name')</th>
            <th>@lang('site.part')</th>
            <th>@lang('site.page')</th>
            <th>@lang('site.speech_number')</th>
            <th>@lang('site.edit')</th>
            <th>@lang('site.delete')</th>
        </tr>
        </thead>

        <tbody>

        @foreach ($narration->indicator_books as $index => $indicator_book)
            <tr id="{{ $indicator_book->pivot->id }}" style="cursor: move;">
                <td>{{ $index + 1 }}</td>
                <td>{{ $indicator_book->name }}</td>
                <td>{{ $indicator_book->pivot->part }}</td>
                <td>{{ $indicator_book->pivot->page }}</td>
                <td>{{ $indicator_book->pivot->speech_number }}</td>
                <td>
                    <button class="btn btn-info btn-sm edit-narration-property"
                            data-url="{{ route('dashboard.narrations.indicator_books.edit', ['narration' => $narration->id, 'indicator_book' => $indicator_book->id, 'pivot_id' => $indicator_book->pivot->id ]) }}"
                            data-title="@lang('site.edit')"
                            style="margin-bottom: 0;"
                    >
                        <i class="fa fa-edit"></i> @lang('site.edit')
                    </button>
                </td>
                <td>
                    <form action="{{ route('dashboard.narrations.indicator_books.destroy', ['narration' => $narration->id, 'indicator_book' => $indicator_book->id, 'pivot_id' => $indicator_book->pivot->id ]) }}" method="post" class="narration-property-form" data-property="indicator_books" style="margin-bottom: 0;">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <button type="submit" class="btn btn-danger btn-sm delete" style="margin-bottom: 0;"><i class="fa fa-trash"></i> @lang('site.delete')</button>
                    </form>
                </td>
            </tr>
        @endforeach

        </tbody>

    </table><!-- end of table -->

    <script>
        $('#indicator_books-table tbody').sortable({
            tolerance: 'pointer',
            axis: "y",
            opacity: 0.50,
            cursor: "move",
            helper: "clone",

            update: function (event, ui) {

                let url = $(this).parent('table').data('url');
                let property = $(this).parent('table').data('property');
                let ids = $(this).sortable('toArray');
                let data = {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'ids': ids
                }

                $.ajax({
                    url: url,
                    method: 'post',
                    data: data,
                    success: function (data) {
                        $('#' + property + ' .accordion-inner').empty().append(data);
                    },
                })
            }
        })
    </script>

@endif