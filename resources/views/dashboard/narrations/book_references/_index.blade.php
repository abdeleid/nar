@include('dashboard.partials._session')

<table class="table table-bordered sort" data-url="{{ route('dashboard.narrations.book_references.sort', $narration->id) }}" data-property="book_references" id="book_references-table">
    <thead>
    <tr>
        <th>#</th>
        <th>@lang('site.name')</th>
        <th>@lang('site.part')</th>
        <th>@lang('site.page')</th>
        <th>@lang('site.edit')</th>
        <th>@lang('site.delete')</th>
    </tr>
    </thead>

    <tbody>

    @foreach ($narration->book_references as $index => $book_reference)

        <tr id="{{ $book_reference->pivot->id }}" style="cursor: move;">
            <td>{{ $index + 1 }}</td>
            <td>{{ $book_reference->name }}</td>
            <td>{{ $book_reference->pivot->part }}</td>
            <td>{{ $book_reference->pivot->page }}</td>
            <td>
                <button class="btn btn-info btn-sm edit-narration-property"
                        data-url="{{ route('dashboard.narrations.book_references.edit', ['narration' => $narration->id, 'book_reference' => $book_reference->id]) }}"
                        data-title="@lang('site.edit')"
                        style="margin-bottom: 0;"
                >
                    <i class="fa fa-edit"></i> @lang('site.edit')
                </button>
            </td>
            <td>
                <form action="{{ route('dashboard.narrations.book_references.destroy', ['narration' => $narration->id, 'book_reference' => $book_reference->id]) }}" method="post" class="narration-property-form" data-property="book_references" style="margin-bottom: 0;">
                    {{ csrf_field() }}
                    {{ method_field('delete') }}
                    <button type="submit" class="btn btn-danger btn-sm" style="margin-bottom: 0;"><i class="fa fa-trash"></i> @lang('site.delete')</button>
                </form>
            </td>
        </tr>
    @endforeach

    </tbody>

</table><!-- end of table -->

<script>
    $('#book_references-table tbody').sortable({
        tolerance: 'pointer',
        axis: "y",
        opacity: 0.50,
        cursor: "move",
        helper: "clone",

        update: function (event, ui) {

            let url = $(this).parent('table').data('url');
            let property = $(this).parent('table').data('property');
            let ids = $(this).sortable('toArray');
            let data = {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'ids': ids
            }

            $.ajax({
                url: url,
                method: 'post',
                data: data,
                success: function (data) {
                    $('#' + property + ' .accordion-inner').empty().append(data);
                },
            })
        }
    })
</script>