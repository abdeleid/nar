<form action="{{ route('dashboard.narrations.book_references.store', $narration->id) }}" class="narration-property-form" data-property="book_references" method="post">

    {{ csrf_field() }}
    {{ method_field('post') }}

    {{--book id--}}
    <div class="form-group">
        <label>@lang('site.book_references')</label>
        <select name="book_reference_id" class="form-control select2">
            <option value="" disabled selected>@lang('site.select')</option>
            @foreach ($book_references as $book_reference)
                <option value="{{ $book_reference->id }}">{{ $book_reference->name }}</option>
            @endforeach
        </select>
    </div>

    {{--name--}}
    <div class="form-group">
        <label>@lang('site.add_new')</label>
        <input type="text" name="name" class="form-control">
    </div>

    {{--part --}}
    <div class="form-group">
        <label>@lang('site.part')</label>
        <input type="number" name="part" class="form-control">
    </div>

    {{--page--}}
    <div class="form-group">
        <label>@lang('site.page')</label>
        <input type="number" name="page" class="form-control">
    </div>

    <div class="form-group">
        <input type="submit" value="@lang('site.add')" class="btn btn-primary">
    </div>

</form><!-- end of form -->