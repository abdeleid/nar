@include('dashboard.partials._session')

<table class="table table-bordered sort" data-url="{{ route('dashboard.narrations.scientists.sort', $narration->id) }}" data-property="scientists" id="scientists-table">
    <thead>
    <tr>
        <th>#</th>
        <th>@lang('site.name')</th>
        <th>@lang('site.edit')</th>
        <th>@lang('site.delete')</th>
    </tr>
    </thead>

    <tbody>

    @foreach ($narration->scientists as $index => $scientist)
        <tr id="{{ $scientist->pivot->id }}" style="cursor: move;">
            <td>{{ $index + 1 }}</td>
            <td>{{ $scientist->name }}</td>
            <td>
                <button class="btn btn-info btn-sm edit-narration-property"
                        data-url="{{ route('dashboard.narrations.scientists.edit', ['narration' => $narration->id, 'scientist' => $scientist->id]) }}"
                        data-title="@lang('site.edit')"
                        style="margin-bottom: 0;"
                >
                    <i class="fa fa-edit"></i> @lang('site.edit')
                </button>
            </td>
            <td>
                <form action="{{ route('dashboard.narrations.scientists.destroy', ['narration' => $narration->id, 'scientist' => $scientist->id]) }}" method="post" class="narration-property-form" data-property="scientists" style="margin-bottom: 0;">
                    {{ csrf_field() }}
                    {{ method_field('delete') }}
                    <button type="submit" class="btn btn-danger btn-sm" style="margin-bottom: 0;"><i class="fa fa-trash"></i> @lang('site.delete')</button>
                </form>
            </td>
        </tr>
    @endforeach

    </tbody>

</table><!-- end of table -->

<script>
    $('#scientists-table tbody').sortable({
        tolerance: 'pointer',
        axis: "y",
        opacity: 0.50,
        cursor: "move",
        helper: "clone",

        update: function (event, ui) {

            let url = $(this).parent('table').data('url');
            let property = $(this).parent('table').data('property');
            let ids = $(this).sortable('toArray');
            let data = {
                '_token': $('meta[name="csrf-token"]').attr('content'),
                'ids': ids
            }

            $.ajax({
                url: url,
                method: 'post',
                data: data,
                success: function (data) {
                    $('#' + property + ' .accordion-inner').empty().append(data);
                },
            })
        }
    })
</script>