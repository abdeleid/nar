<form action="{{ route('dashboard.narrations.scientists.store', $narration->id) }}" class="narration-property-form" data-property="scientists" method="post">

    {{ csrf_field() }}
    {{ method_field('post') }}

    <div class="form-group">
        <label>@lang('site.scientists')</label>
        <select name="scientist_id" class="form-control select2">
            <option value="" disabled selected>@lang('site.select')</option>
            @foreach ($scientists as $scientist)
                <option value="{{ $scientist->id }}">{{ $scientist->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>@lang('site.add_new')</label>
        <input type="text" name="name" class="form-control">
    </div>

    <div class="form-group">
        <input type="submit" value="@lang('site.add')" class="btn btn-primary">
    </div>

</form><!-- end of form -->