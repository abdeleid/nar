<form action="{{ route('dashboard.narrations.scientists.update', ['narration' => $narration->id, 'scientist' => $scientist->id]) }}" class="narration-property-form" data-property="scientists" method="post">

    {{ csrf_field() }}
    {{ method_field('put') }}

    <div class="form-group">
        <label>@lang('site.scientists')</label>
        <select name="scientist_id" class="form-control select2">
            <option value="" disabled>@lang('site.select')</option>
            @foreach ($scientists as $scientist)
                <option value="{{ $scientist->id }}" {{ $scientist->id == $narration_scientist->id ? 'selected' : '' }}>{{ $scientist->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label>@lang('site.add_new')</label>
        <input type="text" name="name" class="form-control">
    </div>
    
    <div class="form-group">
        <input type="submit" value="@lang('site.edit')" class="btn btn-primary">
    </div>

</form><!-- end of form -->