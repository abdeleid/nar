@extends('layouts.dashboard.app')

@section('content')

    <h1 class="page-title">@lang('site.narrations')
        <small>{{ $narrations->total() }}</small>
    </h1>

    {{--breadcrumb--}}
    <ol class="breadcrumb breadcrumb-2">
        <li><a href="{{ route('dashboard.welcome') }}"><i class="fa fa-home"></i>@lang('site.welcome')</a></li>
        <li class="active"><strong>@lang('site.narrations')</strong></li>
    </ol>


    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default">

                <div class="panel-body">

                    <form action="{{ route('dashboard.narrations.index') }}" method="get">

                        <div class="row">
                            {{--<div class="col-md-3">--}}
                            {{--<input type="text" placeholder="@lang('site.search')" name="search" class="form-control" value="{{ request()->search }}" autofocus>--}}
                            {{--</div>--}}

                            <div class="col-md-3">
                                <select name="category_id" class="form-control">
                                    <option value="" selected>@lang('site.all_books')</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}"
                                            {{ request()->category_id == $category->id ? 'selected': '' }}
                                        >
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-3">
                                <input type="number" placeholder="@lang('site.speech_number')" name="speech_number" class="form-control" value="{{ request()->speech_number }}">
                            </div>

                            <div class="col-md-3">
                                <input type="number" placeholder="@lang('site.secondary_number')" name="secondary_number" class="form-control" value="{{ request()->secondary_number }}">
                            </div>

                            <div class="col-md-3">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> @lang('site.search')</button>
                                <a href="{{ route('dashboard.narrations.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</a>
                            </div>

                        </div><!-- end of row -->

                    </form><!-- end of search form -->

                    @if ($narrations->count() > 0)

                        <div class="table-responsive">

                            <table class="table table-hover" style="margin-top: 10px">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>@lang('site.narration_formula')</th>
                                    <th>@lang('site.action')</th>
                                </tr>
                                </thead>

                                <tbody>

                                @php
                                    $index = $narrations->currentPage() * $narrations->perPage() - $narrations->perPage() + 1;
                                @endphp

                                @foreach ($narrations as $narration)

                                    <tr>
                                        <td class="col-md-1">{{ $index++ }}</td>
                                        <td class="col-md-9">{{ $narration->formula() }}</td>
                                        <td class="col-md-2">
                                            <a href="{{ route('dashboard.narrations.edit', $narration->id) }}" class="btn btn-info btn-sm"><i class="fa fa-edit"></i> @lang('site.edit')</a>
                                            <form action="{{ route('dashboard.narrations.destroy', $narration->id) }}" method="post" style="display: inline-block; margin-bottom: 0">
                                                {{ csrf_field() }}
                                                {{ method_field('delete') }}
                                                <button type="submit" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i> @lang('site.delete')</button>
                                            </form><!-- end of form -->
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>

                            </table><!-- end of table -->

                        </div><!-- end of table responsive -->

                        {{ $narrations->appends(request()->query())->links() }}
                    @else

                        <h2>@lang('site.no_data_found')</h2>

                    @endif

                </div><!-- end of panel body -->

            </div><!-- end of panel default -->

        </div><!-- end of col -->

    </div><!-- end of row -->
@endsection
