<form action="{{ route('dashboard.narrations.provisions.update', ['narration' => $narration->id, 'provision' => $provision->id]) }}" class="narration-property-form" data-property="provisions" method="post">

    {{ csrf_field() }}
    {{ method_field('put') }}

    {{--provisions--}}
    <div class="form-group">
        <label>@lang('site.provisions')</label>
        <select name="provision_id" class="form-control select2">
            <option value="" disabled>@lang('site.select')</option>
            @foreach ($provisions as $provision)
                <option value="{{ $provision->id }}" {{ $provision->id == $narration_provision->id ? 'selected' : '' }}>{{ $provision->name }}</option>
            @endforeach
        </select>
    </div>

    {{--name--}}
    <div class="form-group">
        <label>@lang('site.add_new')</label>
        <input type="text" name="name" class="form-control">
    </div>
    
    <div class="form-group">
        <input type="submit" value="@lang('site.edit')" class="btn btn-primary">
    </div>

</form><!-- end of form -->