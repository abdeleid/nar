<form action="{{ route('dashboard.narrations.provisions.store', $narration->id) }}" class="narration-property-form" data-property="provisions" method="post">

    {{ csrf_field() }}
    {{ method_field('post') }}

    {{--provisions--}}
    <div class="form-group">
        <label>@lang('site.provisions')</label>
        <select name="provision_id" class="form-control select2">
            <option value="" disabled selected>@lang('site.select')</option>
            @foreach ($provisions as $provision)
                <option value="{{ $provision->id }}">{{ $provision->name }}</option>
            @endforeach
        </select>
    </div>

    {{--name--}}
    <div class="form-group">
        <label>@lang('site.add_new')</label>
        <input type="text" name="name" class="form-control">
    </div>

    <div class="form-group">
        <input type="submit" value="@lang('site.add')" class="btn btn-primary">
    </div>

</form><!-- end of form -->