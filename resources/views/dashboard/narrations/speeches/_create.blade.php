<form action="{{ route('dashboard.narrations.speeches.store', $narration->id) }}" class="narration-property-form" data-property="speeches" method="post">

    {{ csrf_field() }}
    {{ method_field('post') }}

    {{--categories--}}
    <div class="form-group">
        <label>@lang('site.categories')</label>
        <select name="category_id" class="form-control">
            @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
            @endforeach
        </select>
    </div>

    {{--speech number--}}
    <div class="form-group">
        <label>@lang('site.speech_number')</label>
        <input type="number" name="speech_number" class="form-control">
    </div>

    {{--part number--}}
    <div class="form-group">
        <label>@lang('site.part_number')</label>
        <input type="number" name="part_number" class="form-control">
    </div>

    {{--page number--}}
    <div class="form-group">
        <label>@lang('site.page_number')</label>
        <input type="number" name="page_number" class="form-control">
    </div>

    {{--secondary number--}}
    <div class="form-group">
        <label>@lang('site.secondary_number')</label>
        <input type="number" name="secondary_number" class="form-control">
    </div>

    <div class="form-group">
        <input type="submit" value="@lang('site.add')" class="btn btn-primary">
    </div>

</form><!-- end of form -->