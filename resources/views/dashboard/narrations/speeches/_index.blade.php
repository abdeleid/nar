@include('dashboard.partials._session')


@if ($narration->speeches)

    <table class="table table-bordered sort" data-url="{{ route('dashboard.narrations.speeches.sort', $narration->id) }}" data-property="speeches" id="speeches-table">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('site.category')</th>
            <th>@lang('site.speech_number')</th>
            <th>@lang('site.part_number')</th>
            <th>@lang('site.page_number')</th>
            <th>@lang('site.secondary_number')</th>
            <th>@lang('site.edit')</th>
            <th>@lang('site.delete')</th>
        </tr>
        </thead>

        <tbody>

        @foreach ($narration->speeches as $index => $speech)

            <tr id="{{ $speech->id }}" style="cursor: move;">
                <td>{{ $index + 1 }}</td>
                <td>{{ $speech->category->name }}</td>
                <td>{{ $speech->speech_number }}</td>
                <td>{{ $speech->part_number }}</td>
                <td>{{ $speech->page_number }}</td>
                <td>{{ $speech->secondary_number }}</td>
                <td>
                    <button class="btn btn-info btn-sm edit-narration-property"
                            data-url="{{ route('dashboard.narrations.speeches.edit', ['narration' => $narration->id, 'speech' => $speech->id]) }}"
                            data-title="@lang('site.edit')"
                            style="margin-bottom: 0;"
                    >
                        <i class="fa fa-edit"></i> @lang('site.edit')
                    </button>
                </td>
                <td>
                    <form action="{{ route('dashboard.narrations.speeches.destroy', ['narration' => $narration->id, 'speech' => $speech->id]) }}" method="post" class="narration-property-form" data-property="speeches" style="margin-bottom: 0;">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <button type="submit" class="btn btn-danger btn-sm delete" style="margin-bottom: 0;"><i class="fa fa-trash"></i> @lang('site.delete')</button>
                    </form>
                </td>
            </tr>
        @endforeach

        </tbody>

    </table><!-- end of table -->

    <script>
        $('#speeches-table tbody').sortable({
            tolerance: 'pointer',
            axis: "y",
            opacity: 0.50,
            cursor: "move",
            helper: "clone",

            update: function (event, ui) {

                let url = $(this).parent('table').data('url');
                let property = $(this).parent('table').data('property');
                let ids = $(this).sortable('toArray');
                let data = {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'ids': ids
                }

                $.ajax({
                    url: url,
                    method: 'post',
                    data: data,
                    success: function (data) {
                        $('#' + property + ' .accordion-inner').empty().append(data);
                    },
                })
            }
        })
    </script>

@endif