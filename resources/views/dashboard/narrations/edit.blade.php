@extends('layouts.dashboard.app')

@section('content')

    <style>
        .btn {
            margin-bottom: 1rem;
        }
    </style>

    <h1 class="page-title">@lang('site.narrations')</h1>

    {{--breadcrumb--}}
    <ol class="breadcrumb breadcrumb-2">
        <li><a href="{{ route('dashboard.welcome') }}"><i class="fa fa-home"></i>@lang('site.welcome')</a></li>
        <li><a href="{{ route('dashboard.narrations.index') }}">@lang('site.narrations')</a></li>
        <li class="active"><strong>@lang('site.add')</strong></li>
    </ol>

    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default">

                <div class="panel-body">

                    @php
                        $properties = ['speeches', 'scientist_provisions', 'indicator_books', 'speech_orbits', 'alerts'];
                    @endphp

                    @foreach ($properties as $property)

                        <button
                            class="btn btn-primary add-narration-property"
                            data-url="{{ route('dashboard.narrations.' . $property . '.create', $narration->id) }}"
                            data-title="@lang('site.add') @lang('site.' . $property)"
                        >
                            @lang('site.' . $property)
                        </button>

                    @endforeach

                    @foreach ($properties as $property)

                        <div class="accordion" id="{{ $property }}-wrapper" style="display: {{ count($narration[$property]) > 0 ? 'block' : 'none' }}">

                            <div class="panel accordion-group">

                                <div class="accordion-heading">
                                    <h4 class="title"><a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#{{ $property }}" href="#{{ $property }}">@lang('site.' . $property)</a></h4>
                                </div>

                                <div id="{{ $property }}" class="accordion-body collapse">

                                    <div class="accordion-inner">
                                        @include('dashboard.narrations.' . $property . '._index')
                                    </div>

                                </div><!-- end of collapse one -->

                            </div><!-- end of panel -->

                        </div><!-- end of accordion -->

                    @endforeach

                    <form action="{{ route('dashboard.narrations.update', $narration->id) }}" method="post">

                        {{ csrf_field() }}
                        {{ method_field('put') }}

                        @include('dashboard.partials._errors')

                    </form><!-- end of form -->

                    @foreach ($narration_fields as $narration_field)

                        <h3>{{ $narration_field->name }}</h3>
                        <p>{{ $narration->formula($narration_field->id) }}</p>

                    @endforeach

                </div><!-- end of panel body -->

            </div><!-- end of panel default -->

        </div><!-- end of col -->

    </div><!-- end of row -->
@endsection
