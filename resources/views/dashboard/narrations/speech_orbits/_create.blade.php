<form action="{{ route('dashboard.narrations.speech_orbits.store', $narration->id) }}" class="narration-property-form" data-property="speech_orbits" method="post">

    {{ csrf_field() }}
    {{ method_field('post') }}

    <div class="tabs-container">

        <ul class="nav nav-tabs">
            <li class="active"><a aria-expanded="true" href="#speech_orbit" data-toggle="tab">@lang('site.speech_orbits')</a></li>
            <li><a aria-expanded="false" href="#notes" data-toggle="tab">@lang('site.notes')</a></li>
        </ul>

        <div class="tab-content">

            <div class="tab-pane active" id="speech_orbit">

                <div class="panel-body">

                    <div class="form-group">
                        <label>@lang('site.speech_orbits')</label>
                        <select name="speech_orbit_id" class="form-control select2">
                            <option value="" disabled selected>@lang('site.select')</option>
                            @foreach ($speech_orbits as $speech_orbit)
                                <option value="{{ $speech_orbit->id }}">{{ $speech_orbit->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>@lang('site.add_new')</label>
                        <input type="text" name="name" class="form-control">
                    </div>

                </div><!-- end of panel body -->

            </div><!-- end of tab pane-->

            <div class="tab-pane" id="notes">

                <div class="panel-body">

                    <div class="form-group">
                        <label>@lang('site.notes')</label>
                        <textarea name="notes" class="form-control editable"></textarea>
                    </div>

                </div><!-- end of panel body -->

            </div><!-- end of tab pane -->

        </div><!-- end of tab content -->

    </div><!-- end of tabs -->

    <div class="form-group">
        <input type="submit" value="@lang('site.add')" class="btn btn-primary">
    </div>

</form><!-- end of form -->