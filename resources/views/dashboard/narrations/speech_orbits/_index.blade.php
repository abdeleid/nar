@include('dashboard.partials._session')

@if ($narration->speech_orbits)

    <table class="table table-bordered sort" data-url="{{ route('dashboard.narrations.speech_orbits.sort', $narration->id) }}" data-property="speech_orbits" id="speech_orbits-table">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('site.name')</th>
            <th>@lang('site.notes')</th>
            <th>@lang('site.edit')</th>
            <th>@lang('site.delete')</th>
        </tr>
        </thead>

        <tbody>

        @foreach ($narration->speech_orbits as $index => $speech_orbit)
            <tr id="{{ $speech_orbit->pivot->id }}" style="cursor: move;">
                <td>{{ $index + 1 }}</td>
                <td>{{ $speech_orbit->name }}</td>
                <td>{!! $speech_orbit->pivot->notes !!}</td>
                <td>
                    <button class="btn btn-info btn-sm edit-narration-property"
                            data-url="{{ route('dashboard.narrations.speech_orbits.edit', ['narration' => $narration->id, 'speech_orbit' => $speech_orbit->id]) }}"
                            data-title="@lang('site.edit')"
                            style="margin-bottom: 0;"
                    >
                        <i class="fa fa-edit"></i> @lang('site.edit')
                    </button>
                </td>
                <td>
                    <form action="{{ route('dashboard.narrations.speech_orbits.destroy', ['narration' => $narration->id, 'speech_orbit' => $speech_orbit->id]) }}" method="post" class="narration-property-form" data-property="speech_orbits" style="margin-bottom: 0;">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <button type="submit" class="btn btn-danger btn-sm delete" style="margin-bottom: 0;"><i class="fa fa-trash"></i> @lang('site.delete')</button>
                    </form>
                </td>
            </tr>
        @endforeach

        </tbody>

    </table><!-- end of table -->

    <script>
        $('#speech_orbits-table tbody').sortable({
            tolerance: 'pointer',
            axis: "y",
            opacity: 0.50,
            cursor: "move",
            helper: "clone",

            update: function (event, ui) {

                let url = $(this).parent('table').data('url');
                let property = $(this).parent('table').data('property');
                let ids = $(this).sortable('toArray');
                let data = {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'ids': ids
                }

                $.ajax({
                    url: url,
                    method: 'post',
                    data: data,
                    success: function (data) {
                        $('#' + property + ' .accordion-inner').empty().append(data);
                    },
                })
            }
        })
    </script>

@endif
