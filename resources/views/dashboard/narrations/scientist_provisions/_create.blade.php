<form action="{{ route('dashboard.narrations.scientist_provisions.store', $narration->id) }}" class="narration-property-form" data-property="scientist_provisions" method="post">

    {{ csrf_field() }}
    {{ method_field('post') }}


    <div class="tabs-container">

        <ul class="nav nav-tabs">
            <li class="active"><a aria-expanded="true" href="#scientist" data-toggle="tab">@lang('site.scientists')</a></li>
            <li><a aria-expanded="false" href="#provision" data-toggle="tab">@lang('site.provision')</a></li>
            <li><a aria-expanded="false" href="#book_reference" data-toggle="tab">@lang('site.book_reference')</a></li>
            <li><a aria-expanded="false" href="#notes" data-toggle="tab">@lang('site.notes')</a></li>
        </ul>

        <div class="tab-content">

            <div class="tab-pane active" id="scientist">

                <div class="panel-body">

                    <div class="form-group">
                        <label>@lang('site.scientists')</label>
                        <select name="scientist_id" class="form-control select2">
                            <option value="" disabled selected>@lang('site.select')</option>
                            @foreach ($scientists as $scientist)
                                <option value="{{ $scientist->id }}">{{ $scientist->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>@lang('site.add_new')</label>
                        <input type="text" name="new_scientist" class="form-control">
                    </div>

                </div><!-- end of panel body -->

            </div><!-- end of tab pane-->

            <div class="tab-pane" id="provision">

                <div class="panel-body">

                    <div class="form-group">
                        <label>@lang('site.provision')</label>
                        <select name="provision_id" class="form-control select2">
                            <option value="" disabled selected>@lang('site.select')</option>
                            @foreach ($provisions as $provision)
                                <option value="{{ $provision->id }}">{{ $provision->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>@lang('site.add_new')</label>
                        <input type="text" name="new_provision" class="form-control">
                    </div>

                </div><!-- end of panel body -->

            </div><!-- end of tab pane -->

            <div class="tab-pane" id="book_reference">

                <div class="panel-body">

                    <div class="form-group">
                        <label>@lang('site.book_reference')</label>
                        <select name="book_reference_id" class="form-control select2">
                            <option value="" disabled selected>@lang('site.select')</option>
                            @foreach ($book_references as $book_reference)
                                <option value="{{ $book_reference->id }}">{{ $book_reference->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label>@lang('site.add_new')</label>
                        <input type="text" name="new_book_reference" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>@lang('site.part')</label>
                        <input type="text" name="book_reference_part" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>@lang('site.page')</label>
                        <input type="text" name="book_reference_page" class="form-control">
                    </div>

                </div><!-- end of panel body -->

            </div><!-- end of tab pane -->

            <div class="tab-pane" id="notes">

                <div class="panel-body">

                    <div class="form-group">
                        <label>@lang('site.notes')</label>
                        <textarea name="notes" class="form-control editable"></textarea>
                    </div>

                </div><!-- end of panel body -->

            </div><!-- end of tab pane -->

        </div><!-- end of tab content -->

    </div><!-- end of tabs -->

    <div class="form-group">
        <input type="submit" value="@lang('site.add')" class="btn btn-primary">
    </div>

</form><!-- end of form -->