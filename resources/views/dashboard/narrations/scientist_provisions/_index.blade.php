@include('dashboard.partials._session')

@if ($narration->scientist_provisions)

    <table class="table table-bordered sort" data-url="{{ route('dashboard.narrations.scientist_provisions.sort', $narration->id) }}" data-property="scientist_provisions" id="scientist_provisions-table">
        <thead>
        <tr>
            <th>#</th>
            <th>@lang('site.scientist')</th>
            <th>@lang('site.provision')</th>
            <th>@lang('site.book_reference')</th>
            <th>@lang('site.part')</th>
            <th>@lang('site.page')</th>
            <th>@lang('site.notes')</th>
            <th>@lang('site.edit')</th>
            <th>@lang('site.delete')</th>
        </tr>
        </thead>

        <tbody>

        @foreach ($narration->scientist_provisions as $index => $scientist_provision)

            <tr id="{{ $scientist_provision->id }}" style="cursor: move;">
                <td>{{ $index + 1 }}</td>
                <td>{{ $scientist_provision->scientist->name }}</td>
                <td>{{ $scientist_provision->provision->name }}</td>
                <td>{{ $scientist_provision->book_reference->name }}</td>
                <td>{{ $scientist_provision->book_reference_part }}</td>
                <td>{{ $scientist_provision->book_reference_page }}</td>
                <td>{!! $scientist_provision->notes !!}</td>
                <td>
                    <button class="btn btn-info btn-sm edit-narration-property"
                            data-url="{{ route('dashboard.narrations.scientist_provisions.edit', ['narration' => $narration->id, 'scientist_provision' => $scientist_provision->id]) }}"
                            data-title="@lang('site.edit')"
                            style="margin-bottom: 0;"
                    >
                        <i class="fa fa-edit"></i> @lang('site.edit')
                    </button>
                </td>
                <td>
                    <form action="{{ route('dashboard.narrations.scientist_provisions.destroy', ['narration' => $narration->id, 'scientist_provision' => $scientist_provision->id]) }}" method="post" class="narration-property-form" data-property="scientist_provisions" style="margin-bottom: 0;">
                        {{ csrf_field() }}
                        {{ method_field('delete') }}
                        <button type="submit" class="btn btn-danger delete btn-sm" style="margin-bottom: 0;"><i class="fa fa-trash"></i> @lang('site.delete')</button>
                    </form>
                </td>
            </tr>
        @endforeach

        </tbody>

    </table><!-- end of table -->

    <script>
        $('#scientist_provisions-table tbody').sortable({
            tolerance: 'pointer',
            axis: "y",
            opacity: 0.50,
            cursor: "move",
            helper: "clone",

            update: function (event, ui) {

                let url = $(this).parent('table').data('url');
                let property = $(this).parent('table').data('property');
                let ids = $(this).sortable('toArray');
                let data = {
                    '_token': $('meta[name="csrf-token"]').attr('content'),
                    'ids': ids
                }

                $.ajax({
                    url: url,
                    method: 'post',
                    data: data,
                    success: function (data) {
                        $('#' + property + ' .accordion-inner').empty().append(data);
                    },
                })
            }
        })
    </script>
@endif


