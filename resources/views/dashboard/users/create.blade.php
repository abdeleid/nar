@extends('layouts.dashboard.app')

@section('content')

    <style>
        .btn {
            margin-bottom: 1rem;
        }
    </style>

    <h1 class="page-title">@lang('site.users')</h1>

    {{--breadcrumb--}}
    <ol class="breadcrumb breadcrumb-2">
        <li><a href="{{ route('dashboard.welcome') }}"><i class="fa fa-home"></i>@lang('site.welcome')</a></li>
        <li><a href="{{ route('dashboard.users.index') }}">@lang('site.users')</a></li>
        <li class="active"><strong>@lang('site.add')</strong></li>
    </ol>

    <div class="row">

        <div class="col-md-12">

            <div class="panel panel-default">

                <div class="panel-body">
                    
                    <form action="{{ route('dashboard.users.store') }}" method="post">

                        {{ csrf_field() }}
                        {{ method_field('post') }}

                        @include('dashboard.partials._errors')

                        {{--name--}}
                        <div class="form-group">
                            <label>@lang('site.name')</label>
                            <input type="text" name="name" value="{{ $user->name ?? old('name') }}" class="form-control">
                        </div>

                        {{--email--}}
                        <div class="form-group">
                            <label>@lang('site.email')</label>
                            <input type="email" name="email" value="{{ $user->email ?? old('email') }}" class="form-control">
                        </div>

                        {{--password--}}
                        <div class="form-group">
                            <label>@lang('site.password')</label>
                            <input type="password" name="password" class="form-control">
                        </div>

                        {{--confirm password--}}
                        <div class="form-group">
                            <label>@lang('site.password_confirmation')</label>
                            <input type="password" name="password_confirmation" class="form-control">
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> @lang('site.add')</button>
                        </div>

                    </form><!-- end of form -->

                </div><!-- end of panel body -->

            </div><!-- end of panel default -->

        </div><!-- end of col -->

    </div><!-- end of row -->
@endsection
