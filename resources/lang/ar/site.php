<?php

return [
    'welcome' => 'الرئسيه',
    'logout' => 'تسجيل الخروج',
    'login' => 'تسجيل الدخول',
    'email' => 'البريد الاكتروني',
    'password' => 'كلمة السر',
    'password_confirmation' => 'تاكيد كلمه السر',
    'all_books' => 'الكل الكتب',

    'add' => 'اضف',
    'add_new' => 'اضف جديد',
    'edit' => 'تعديل',
    'delete' => 'حذف',
    'action' => 'اكشن',
    'search' => 'بحث',
    'select' => 'اختر',
    'no_data_found' => 'للاسف لايوحد اي سجلات',
    'added_successfully' => 'تم اضافه السجلات بنجاح',
    'updated_successfully' => 'تم تعديل السجلات بنجاح',
    'sorted_successfully' => 'تم ترتيب السجلات بنجاح',
    'deleted_successfully' => 'تم حذف السجلات بنجاح',
    'confirm_delete' => 'تاكيد الحذف',
    'yes' => 'نعم',
    'no' => 'لا',

    'categories' => 'كتب',
    'category' => 'كتاب',
    'name' => 'الاسم',
    'number_of_speeches' => 'عدد الاحاديث',
    'missing_speeches' => 'الاحاديث الناقصه',

    'narrations' => 'التخريجات',
    'narration_formula' => 'صيغه التخريج',

    'speeches' => 'لاحاديث',
    'speech_number' => ' رقم الحديث',
    'secondary_number' => ' الرقم الفرعي',
    'part_number' => 'رقم الجزء',
    'page_number' => ' الرقم الصفحه',

    'scientists' => 'العلماء',
    'scientist' => 'العالم',

    'book_references' => 'مراجع الكتب',
    'book_reference' => 'مرجع الكتاب',
    'part' => 'الجزء',
    'page' => 'الصفحه',

    'provisions' => 'احكام',
    'provision' => 'الحكم',

    'scientist_provisions' => 'احكام اهل العلم',
    'notes' => 'ملاحظات',

    'indicator_books' => 'كتب الاطراف',
    'numbers' => 'الارقام',

    'speech_orbits' => 'مدارات الحديث',

    'narration_fields' => 'مجالات التخريج',
    'narration_field' => 'مجال التخريج',

    'classifications' => 'التصنيفات',
    'classification' => 'التصنيف',
    'alerts' => 'التنبيهات',
    'alert' => 'التنبيه',
    'ref' => 'المرجع',
    'det' => 'التفاصيل',
    'title' => 'العنوان',

    'users' => 'المستخدمين',
    'user' => 'المستخدم',


];