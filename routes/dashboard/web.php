<?php

Route::prefix('dashboard')
    ->name('dashboard.')
    ->middleware(['auth', 'role:super_admin'])
    ->group(function () {

        //migration routes
        Route::get('/migrations', 'MigrationController')->name('migrations');

        Route::get('/', 'WelcomeController@index')->name('welcome');

        //category routes
        Route::post('categories/sort', 'Category\CategoryController@sort')->name('categories.sort');
        Route::resource('categories', 'Category\CategoryController');
        Route::get('categories/{category}/missing_speeches', 'Category\MissingSpeechController@index')->name('categories.missing_speeches.index');

        //scientist routes
        Route::resource('scientists', 'ScientistController');

        //book reference routes
        Route::resource('book_references', 'BookReferenceController');

        //provision routes
        Route::resource('provisions', 'ProvisionController');

        //indicator book routes
        Route::resource('indicator_books', 'IndicatorBookController');

        //speech orbit routes
        Route::resource('speech_orbits', 'SpeechOrbitController');

        //narration field controller
        Route::resource('narration_fields', 'NarrationFieldController');

        //classification routes
        Route::resource('classifications', 'ClassificationController');

        //user routes
        Route::resource('users', 'UserController');

        //narration routes
        Route::namespace('Narration')->group(function () {

            //narration routes
            Route::resource('narrations', 'NarrationController');

            //speech routes
            Route::post('narrations/{narration}/speeches/sort', 'SpeechController@sort')->name('narrations.speeches.sort');
            Route::resource('narrations.speeches', 'SpeechController');

            //scientist routes
            Route::post('narrations/{narration}/scientists/sort', 'ScientistController@sort')->name('narrations.scientists.sort');
            Route::resource('narrations.scientists', 'ScientistController');

            //book reference routes
            Route::post('narrations/{narration}/book_references/sort', 'BookReferenceController@sort')->name('narrations.book_references.sort');
            Route::resource('narrations.book_references', 'BookReferenceController');

            //provisions routes
            Route::post('narrations/{narration}/provisions/sort', 'ProvisionController@sort')->name('narrations.provisions.sort');
            Route::resource('narrations.provisions', 'ProvisionController');

            //scientist provision routes
            Route::post('narrations/{narration}/scientist_provisions/sort', 'ScientistProvisionController@sort')->name('narrations.scientist_provisions.sort');
            Route::resource('narrations.scientist_provisions', 'ScientistProvisionController');

            //book indicator routes
            Route::post('narrations/{narration}/indicator_books/sort', 'IndicatorBookController@sort')->name('narrations.indicator_books.sort');
            Route::resource('narrations.indicator_books', 'IndicatorBookController');

            //speech orbit routes
            Route::post('narrations/{narration}/speech_orbits/sort', 'SpeechOrbitController@sort')->name('narrations.speech_orbits.sort');
            Route::resource('narrations.speech_orbits', 'SpeechOrbitController');

            //alert routes
            Route::post('narrations/{narration}/alerts/sort', 'AlertController@sort')->name('narrations.alerts.sort');
            Route::resource('narrations.alerts', 'AlertController');

        });

    });
