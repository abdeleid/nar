$(document).ready(function () {

    $(document).on('click', '.add-narration-property, .edit-narration-property', function () {

        $('#narration-property-error').empty();
        $('#narration-property-error').css('display', 'none');

        var title = $(this).data('title');
        var url = $(this).data('url');
        $('#narration-property-modal').modal('show');
        $('#narration-property-modal .modal-body-inner').empty();

        $.ajax({
            url: url,
            success: function (data) {

                $('#narration-property-modal .modal-title').text(title);
                $('#narration-property-modal .modal-body .modal-body-inner').append(data);
                $(".select2").select2({
                    width: '100%',
                    dir: 'rtl',
                });

                $(".taggable").tagsinput({
                    trimValue: true,
                });

                $('.bootstrap-tagsinput input').keydown(function (event) {
                    if (event.which == 13) {
                        $(this).blur();
                        $(this).focus();
                        return false;
                    }
                })

            }//end of success

        });//end of ajax

    });//end of add edit narration property

    $(document).on('submit', '.narration-property-form', function (e) {

        e.preventDefault();

        var that = $(this);
        var url = that.attr('action');
        var method = that.find('input[name="_method"]').val();
        var data = that.serialize();
        var property = that.data('property');

        $.ajax({
            url: url,
            method: method,
            data: data,
            success: function (data) {

                $('.taggable').tagsinput('removeAll');

                for (name in CKEDITOR.instances) {
                    CKEDITOR.instances[name].destroy();
                }

                that.trigger('reset');

                $(".select2").val('').trigger('change');

                $('#narration-property-error').empty();
                $('#narration-property-error').css('display', 'none');

                $('#' + property + '-wrapper').css('display', 'block');
                $('#' + property + ' .accordion-inner').empty().append(data);

                if (method == 'post') { //for store

                    $('#' + property).collapse('show');

                } else { //for update

                    $('#narration-property-modal').modal('hide');

                }//end of else

                if ($('#' + property + '-table tbody').children().length == 0) {
                    $('#' + property + '-wrapper').css('display', 'none');
                }

            },
            error: function (data) {

                $('#narration-property-error').css('display', 'block');
                $('#narration-property-error').empty();

                var errors = data.responseJSON.errors;

                for (var key in errors) {

                    $('#narration-property-error').append(errors[key][0] + '<br>');

                } //end of for

            }//end of error

        });//end of ajax

    });//end of narration property form submit

});//end of document ready
